<?php

namespace app\controllers;

use Yii;
use app\components\AccessRule;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\ContactForm;
use yii\base\Controller;
use app\models\Settings;
use app\components\AppSetting;
use app\modules\user\models\User;
use app\components\AppInterface;
use app\components\AppMessages;
use yii2tech\filestorage\local\Storage;
use app\modules\user\components\AppUser;
use app\modules\product\models\ProductType;
use app\modules\product\models\Product;
use app\models\Country;
use app\models\State;
use app\models\City;
use app\modules\product\models\Shipment;

class SiteController extends \yii\web\Controller {

    public function actionDashboard() {
        $user = AppUser::getCurrentUser();
        if (isset($user->id)) {
            $layout = $user->type == 'customer' ? 'dashboard' : 'default';
        } else {
            $layout = 'dashboard';
        }
        $this->layout = \Yii::$app->params['layout_path'] . $layout;
        if (isset($_GET['search'])) {
            $products = Product::find()
                            ->where(['LIKE', 'title', $_GET['search']])
                            ->orWhere(['LIKE', 'description', $_GET['search']])
                            ->orderby(['modified_at' => SORT_ASC])->all();
        } else {
            $products = Product::find()->where(['is_deleted' => 0])->orderby(['modified_at' => SORT_ASC])->all();
        }
//        dd($products);
        $types = ProductType::find()->orderby(['modified_at' => SORT_ASC])->all();

        if (isset(Yii::$app->session['order'])) {
            $notInCondition = implode(",", Yii::$app->session['order']);
            $products = Product::find()->where(['is_deleted' => 0])->andWhere(['NOT IN', 'id', $notInCondition])->orderby(['modified_at' => SORT_DESC])->all();
        }

        $offers = \app\modules\product\models\Offers::find()->where(['is_deleted' => 0])->all();
        $offer_arr = [];
        foreach ($offers as $data) {
            $offer_product = \app\modules\product\models\OfferProduct::find()->where(['offer_id' => $data['id']])->all();
            foreach ($offer_product as $key => $item) {
                if ($data['status'] == 'active') {
                    $offer_arr[$key] = $item['product_id'];
                }
            }
        }
//        dd($offer_arr);

        return $this->render('dashboard', array('types' => $types, 'products' => $products, 'offers' => $offer_arr));
    }

    public function actionCart() {
//        dd();
        if($_POST){
//            dd($_POST);
             if (count(Yii::$app->session['unit']) == 0) {
                $_SESSION['unit'] = array();
            }
            $_SESSION['unit'][$_POST['product']]=$_POST['unit'];
        }
//        dd(Yii::$app->session['unit']);
        $user = AppUser::getCurrentUser();
        $recommendations = '';
        if (!empty($user)) {
            $recommendations = AppUser::getRecommendations();
        }
        $this->layout = \Yii::$app->params['layout_path'] . 'dashboard';
        Yii::$app->session['order'];
        if (isset($_GET['id']) && $_GET['id'] != '') {
            unset($_SESSION['order'][$_GET['id']]);
        }
        $offers = \app\modules\product\models\Offers::find()->where(['is_deleted' => 0])->all();
        $offer_arr = [];
        foreach ($offers as $data) {
            $offer_product = \app\modules\product\models\OfferProduct::find()->where(['offer_id' => $data['id']])->all();
            foreach ($offer_product as $key => $item) {
                if ($data['status'] == 'active') {
                    $offer_arr[$key] = $item['product_id'];
                }
            }
        }

        return $this->render('cart', array('order' => Yii::$app->session['order']
                    , 'recommendations' => $recommendations, 'offers' => $offer_arr
        ));
    }

    public function actionCheckout() {
        $this->layout = \Yii::$app->params['layout_path'] . 'dashboard';
        if (AppUser::isUserLogin()) {
            $model = User::find()->where(['id' => AppUser::getUserId()])->one();
        } else {
            \Yii::$app->getSession()->setFlash('error', 'You must login or register to continue shopping..');
            Yii::$app->response->redirect(['/user/main/login']);
        }
        $countries = Country::find()->all();
        $states = State::find()->all();
        $cities = City::find()->all();
        if ($model->load(\Yii::$app->request->post())) {
//            dd($_POST);
            $checkout = AppUser::createCheckout($_POST['User']);
            if ($checkout) {
                Yii::$app->response->redirect(['/site/success']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$add_failure);
            }
        }

        return $this->render('checkout', array('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
    }

    public function actionSuccess() {
        $this->layout = \Yii::$app->params['layout_path'] . 'dashboard';
        Yii::$app->session->remove('order');
        $model = \app\modules\product\models\Shipment::find()->where(['customer_id' => AppUser::getCurrentUser()->id])->orderby(['modified_at' => SORT_DESC])->all();
        $order = \app\modules\product\models\Order::find()->where(['order_id' => $model[0]['order_id']])->all();

        $offers = \app\modules\product\models\Offers::find()->where(['is_deleted' => 0])->all();
        $offer_arr = [];
        foreach ($offers as $data) {
            $offer_product = \app\modules\product\models\OfferProduct::find()->where(['offer_id' => $data['id']])->all();
            foreach ($offer_product as $key => $item) {
                if ($data['status'] == 'active') {
                    $offer_arr[$key] = $item['product_id'];
                }
            }
        }

        return $this->render('success', array('model' => $model, 'order' => $order, 'offers' => $offer_arr));
    }

    public function actionAddtocart() {
        $this->layout = \Yii::$app->params['layout_path'] . 'dashboard';
        $id = '';
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $count = count(Yii::$app->session['order']);
            if (count(Yii::$app->session['order']) == 0) {
                $_SESSION['order'] = array();
            }
            array_push($_SESSION['order'], $id);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function actionLogin() {
        return \Yii::$app->getResponse()->redirect(\yii\helpers\Url::to('../user/main/login'));

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAddgift() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new \app\models\Gifts();
        $products = Product::find()->where(['is_deleted' => 0])->all();
        if ($_POST) {
            $model->id = AppInterface::getUniqueID();
            $model->attributes = $_POST['Gifts'];
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', AppMessages::$create_success);
                return $this->redirect(['giftindex']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$error);
            }
        }
        return $this->render('addgift', array('model' => $model, 'products' => $products));
    }

    public function actionGiftindex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\models\Gifts::find()->where(['is_deleted' => 0])->orderby(['modified_at' => SORT_DESC])->all();
        return $this->render('gift_index', array('model' => $model));
    }

    public function actionGiftdelete($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\models\Gifts::find()->where(['id' => $id])->one();
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect(['giftindex']);
    }

    public function actionLuckydraws() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $gifts = \app\models\Gifts::find()->where(['is_deleted' => 0])->all();
        $user = User::find()->where(['is_deleted' => 0, 'type' => 'customer'])->all();
        $random = rand(1, count($user));
        if ($_POST) {
            $model = new \app\models\GiftUser();
            $model->id = AppInterface::getUniqueID();
            $model->product_id = $_POST['Gifts']['product_id'];
            $model->user_id = $user[$random - 1]['id'];
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', AppMessages::$create_success);
                return $this->redirect(['giftuser']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$error);
            }
        }

        return $this->render('luckydraws', array('gifts' => $gifts));
    }

    public function actionGiftuser() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\models\GiftUser::find()->where(['is_deleted' => 0])->all();
        return $this->render('gift_user', array('model' => $model));
    }

    public function actionUsergiftdelete($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\models\GiftUser::find()->where(['id' => $id])->one();
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect(['giftuser']);
    }

}
