<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CDbMigrationSP
 *
 * @author SAKIB
 */
class CDbMigrationSP extends Migration {
    //put your code here
    
    public function _addColumn($table, $column, $type) {
 
      // Fetch the table schema
      $table_to_check = Yii::app()->db->schema->getTable($table);
      if ( ! isset( $table_to_check->columns[$column] )) {
        $this->addColumn($table, $column, $type);
      }
 
    }
    
    public function _createTable($table, $columns, $options = NULL) {
      // Fetch the table schema
      $table_to_check = Yii::app()->db->schema->getTable($table);
      if ( ! is_object($table_to_check)) {
        $this->createTable($table, $columns, $options);
        return TRUE;
      }
      return FALSE;
    }
    
    public function isTableExst($table){
   
     $table_to_check = Yii::app()->db->schema->getTable($table);
     
      if ( ! is_object($table_to_check)) {
        return false;
      }else{
        return true;
      }
    }
    
     public function isColExst($table,$column){
      
      $table_to_check = Yii::app()->db->schema->getTable($table);
      if ( ! isset( $table_to_check->columns[$column] )) {
         return false;
      }  
         
      return true;
    }
    
}
