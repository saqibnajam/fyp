
var alrt = 'Alert!!';
var error = 'Error!!';
var pwd_match = 'Your password does not match';
var address_country = 'Address and country are required';
var select_country = 'Select any country';
var select_role = 'Select any role';



jQuery(document).ready(function() {
///*** Use JAVASCRIPT constants here in selector as well as for messages/text 

    // Tags Input
//    jQuery('#tags').tagsInput({width: 'auto'});
    // Form Toggles
    jQuery('.toggle').toggles({on: true});

    // Date Picker
    jQuery('.datepicker').datepicker({
        autoclose: true,
        format: 'd-M-yyyy'
    });
    jQuery('.dob').datepicker({
        startView: 2,
        autoclose: true,
        format: 'd-M-yyyy'
    });
    jQuery('#datepicker-inline').datepicker();
    jQuery('#datepicker-multiple').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
    });

    jQuery('.summernote').summernote({
        height: 200, // set editor height

        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor

        focus: true                 // set focus to editable area after initializing summernote
    });


//    ============  FORMS VALIDATION ACTION CALL ON DOCUMENT READY ===========
    $("#password_form").validate();
    $("#user_form").validate();
    $("#buss_form").validate();
    $("#keyword_form").validate();
    $("#feeds_form").validate();
    $("#offer_form").validate();
    $("#device_form").validate();

});

function checkPwd() {
    var pwd = $('#pwd').val();
    var new_pwd = $('#new_pwd').val();
    if (pwd == new_pwd) {
        return true;
    } else {
        swal(error, pwd_match);
        return false;
    }
}

 function validateUser() {
    var flag = false;
    var country = validateField('country', select_country);
    var role = validateField('role', select_role);
    if (country == true && role == true) {
        flag = true;
    } else {
        flag = false;
    }
    return flag;
}

function validateBusiness() {
    var flag = false;
    var country = validateField('country', select_country);
    var long = $('#longitude').val();
    var lat = $('#latitude').val();
    if (long != '' && lat != '' && country == true) {
        flag = true;
    } else {
        swal(alrt, address_country);
        flag = false;
    }
    return flag;
}

function validateField(id, msg) {
    var field = $('#' + id).val();
    if (field != '0') {
        return true;
    } else {
        swal(alrt, msg);
        return false;
    }
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

///*** Function name needs to be changed also handle JS CONSTANTS
function roleChange() {
    var role = $('#role').val();
    if (role == '2' || role == '3') {
        $("#business_id").css("display", "block");
        $("#select-business").prop('disabled', false);
    } else {
        $("#business_id").css("display", "none");
        $("#select-business").prop('disabled', true);
    }
}
///***