<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Checkout'));
?>

<!-- Vertical Steps Example -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <!--<div class="panel-heading">--> 
            <!--<h3 class="panel-title">Vertical Steps Example</h3>--> 
            <!-- Page-Title -->
            <?php
//                echo $this->render('_title', array('type' => 'Checkout'));
            ?>

            <!--</div>--> 
            <div class="panel-body"> 
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateCustomer();', 'id' => 'customer_form']]); ?>
                <div id="wizard-vertical">
                    <h3>Customer Detail</h3>
                    <section>
                        <label class="col-md-2 control-label">First Name</label>                
                        <div class="col-md-10">
                            <?php
                            echo $form->field($model, 'f_name')
                                    ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <label class="col-md-2 control-label">Last Name</label>
                        <div class="col-md-10">
                            <?php
                            echo $form->field($model, 'l_name')
                                    ->textInput(array('placeholder' => 'Last Name'))->label(false);
                            ?>
                        </div>
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <?php
                            echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
                            ?>
                        </div>
                        <?php if ($model->isNewRecord) { ?>
                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-10">
                                <?php echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                            </div>
                        <?php } ?>
                        <label class="col-md-2 control-label">Phone</label>
                        <div class="col-md-10">
                            <?php
                            echo $form->field($model, 'phone')->textInput(
                                            array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                                    ->label(false);
                            ?>
                        </div>
                    </section>
                    <h3>Shipping Detail</h3>
                    <section>
                        <div class="col-md-12">
                            <label class="col-md-2 control-label">Address</label>
                            <div class="col-md-10">
                                <?php
                                echo $form->field($model, 'address')->textInput(
                                                array('placeholder' => 'Address'))
                                        ->label(false);
                                ?>                            
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" >
                                <label class="col-md-2 control-label">Country</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="User[country_id]" id="country_id" onchange="getstate(this.value)">
                                        <option value="" >Select Any Country</option>
                                        <?php foreach ($countries as $data) { ?>
                                            <option <?php echo isset($model->country_id) && $model->country_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br><br><br><br><br>

                        <div class="col-md-12">
                            <div class="form-group" >
                                <label class="col-md-2 control-label">State</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="User[state_id]" id="state" onchange="getcity(this.value)">
                                        <option value="" >Select Any State</option>
                                        <?php foreach ($states as $data) { ?>
                                            <option <?php echo isset($model->state_id) && $model->state_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="col-md-12" style="margin: 10px 0;">
                            <label class="col-md-2 control-label">City</label>
                            <div class="col-md-10">
                                <select class="form-control" name="User[city_id]" id="city">
                                    <option value="" >Select Any City</option>
                                    <?php foreach ($cities as $data) { ?>
                                        <option <?php echo isset($model->city_id) && $model->city_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </section>
                    <h3>Payment Detail</h3>
                    <section>
                        <div class="row">
                            <div class="radio radio-primary">
                                <input type="radio" name="User[payment_type]" id="radio3" value="card">
                                <label for="radio3">
                                    Debit or Credit Card
                                </label>
                            </div>
                            <div id="Method" class="desc" style="display: none;">
                                <div style="margin: -16px 0 0 16px;">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label class="control-label"><em>*</em>Credit Card Type</label>
                                            <select name="User[card_type]" class="form-control">
                                                <option value="Visa Card">Visa Card</option>
                                                <option value="Visa Card">Master Card</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="control-label"><em>*</em>Credit Card Number</label>
                                            <input type="text" name="User[card_number]" title="Credit Card Number" class="form-control" value="">
                                        </div>                                       
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <!--<div class="col-md-6">-->
                                            <label class="control-label"><em>*</em>Expiration Date</label>    
                                            <!--</div>-->
                                            <br>
                                            <div class="col-md-6">
                                                <select name="User[expiry_month]" class="form-control">
                                                    <option value="" selected="selected">Month</option>
                                                    <option value="January">01 - January</option>
                                                    <option value="February">02 - February</option>
                                                    <option value="March">03 - March</option>
                                                    <option value="April">04 - April</option>
                                                    <option value="May">05 - May</option>
                                                    <option value="June">06 - June</option>
                                                    <option value="July">07 - July</option>
                                                    <option value="Auguest">08 - August</option>
                                                    <option value="September">09 - September</option>
                                                    <option value="October">10 - October</option>
                                                    <option value="November">11 - November</option>
                                                    <option value="December">12 - December</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="User[expiry_year]" class="form-control">
                                                    <option value="" selected="selected">Year</option>
                                                    <option value="2017">2017</option>
                                                    <option value="2018">2018</option>
                                                    <option value="2019">2019</option>
                                                    <option value="2020">2020</option>
                                                    <option value="2021">2021</option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                    <option value="2027">2027</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label"><em>*</em>Card Verification Number</label>
                                            <input type="text" title="Card Verification Number" class="form-control" name="User[card_varification]" value="">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="radio radio-primary">
                                <input type="radio" name="User[payment_type]" id="radio4" value="cash" checked="checked">
                                <label for="radio4">
                                    Cash on Delivery
                                </label>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $("input[name$='User[payment_type]']").click(function () {
                                    var method = $(this).val();
                                    if (method === 'card') {
                                        $("#Method").show();
                                    } else {
                                        $("#Method").hide();
                                    }
                                });
                            });
                        </script>
                    </section>
                    <h3>Finish</h3>
                    <section>
                        <label class="col-md-2 control-label">Comment</label>
                        <div class="col-md-10">
                            <?php
                            echo $form
                                    ->field($model, 'description')
                                    ->textarea(array('placeholder' => 'Any Comment About Project', 'class' => 'form-control'))
                                    ->label(false);
                            ?>
                        </div>

                        <div class="form-group clearfix">
                            <div class="col-lg-12">
                                <div class="checkbox checkbox-primary">
                                    <div class="col-md-2 control-label"></div>
                                    <input id="checkbox-v" checked="checked" type="checkbox">
                                    <label for="checkbox-v">
                                        I agree with the Terms and Conditions.
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-10"> </div>
                            </div>
                            <div class="col-md-12"> 
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-10"> </div>
                            </div>
                            <div class="col-md-12"> 
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-10"> </div>
                            </div>
                            <div class="col-md-12"> 
                                <div class="col-md-9"> 
                                    <p><strong>Note: </strong> Its a Free Shipment for login customers..</p>
                                </div>
                                <div class="col-md-3"> 
                                    <?php
                                    echo Html::submitButton('Place Order', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                                    ?>

                                </div>
                            </div>
                    </section>
                </div> <!-- End #wizard-vertical -->
                <?php ActiveForm::end(); ?>     
            </div>  <!-- End panel-body -->
        </div> <!-- End panel -->
    </div> <!-- end col -->
</div> <!-- End row -->


<script>
    function getstate(id) {
        var url = "" + "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/user/main/getstate?name=User[state_id]";
        $.ajax({url: url,
            data: {country_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $('#state').html(result);
<?php if (isset($model->state_id)) { ?>
                    $("#state").val('<?php echo $model->state_id; ?>');
<?php } ?>
            },
            error: function () {
                alert('Error occured');
            }
        });
    }
    function getcity(id) {
        var url = "<?php echo Yii::$app->urlManager->baseUrl; ?>" + "/user/main/getcity?name=User[city_id]";
        $.ajax({url: url,
            data: {state_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $('#city').html(result);
<?php if (isset($model->city_id)) { ?>
                    $("#city").val('<?php echo $model->city_id; ?>');
<?php } ?>
            },
            error: function () {
                alert('Error occured');
            }
        });
    }

</script>