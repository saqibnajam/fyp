<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Lucky draws'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-horizontal" >
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateProduct();', 'id' => 'buss_form']]); ?>
                            <div class="form-group field-producttype-title required">
                                <label class="col-sm-2 control-label">Gift</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="Gifts[product_id]">
                                        <option value="" >Select Gift</option>
                                        <?php foreach ($gifts as $type) { ?>
                                            <option value="<?php echo $type->product_id; ?>" > <?php echo $type->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                    
                            </div>                    

                            <label class="col-md-2 control-label"></label>
                            <div class="col-md-2">
                                <?php
                                echo Html::submitButton('Search', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                                ?>
                            </div>
                            <?php ActiveForm::end(); ?>     
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
