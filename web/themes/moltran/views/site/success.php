<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\User;
use app\modules\user\models\Role;
use app\models\Country;
use app\modules\product\models\Product;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
//echo $this->render('_title', array('type' => '', 'index' => 'user/main/index'));
//dd($model[0]['order_id']);
?>
<!-- Page-Title -->
<!--<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">Invoice</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="#">Moltran</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Invoice</li>
        </ol>
    </div>
</div>-->

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Invoice</h4>
            </div> 
            <div class="panel-body">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-right"><img src="<?php echo AppInterface::getBaseUrl(); ?>/assets/images/shoppingcart2.png"
                                                    alt="<?php echo AppInterface::getAppName(); ?>" title="<?php echo AppInterface::getAppName(); ?>" style="height: 50px;width: 185px;"></h4>

                    </div>
                    <div class="pull-right">
                        <h4>Order # <br>
                            <strong><?php echo $model[0]['id']; ?></strong>
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left m-t-30 col-md-6">
                            <address>
                                <strong>Shipping Address: </strong><br>
                                <br>
                                <?php echo $model[0]['address']; ?>
                                <br>
                                <abbr title="Phone">Phone #:</abbr> <?php echo $model[0]['phone']; ?>
                            </address>
                        </div>
                        <div class="pull-right m-t-30">
                            <p><strong>Order Date: </strong> <?php echo date('d-M-Y', $model[0]['issue_date']); ?></p>
                            <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-pink">Pending</span></p>
                            <p class="m-t-10"><strong>Order ID: </strong> #<?php echo $model[0]['order_id']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="m-h-50"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table" id="_datatable-editable">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $total = 0;
                                    if (isset($order) && $order != '') {
                                        foreach ($order as $key => $data) {
//                                            dd($data->product_id);
                                            $product = Product::find()->where(['is_deleted' => 0, 'id' => $data->product_id])->one();
                             $unit = 1;
                        if (isset(Yii::$app->session['unit']) && Yii::$app->session['unit'] != '') {
                            foreach (Yii::$app->session['unit'] as $key1 => $data1) {
                                if ($key1 == $product->id) {
                                    $unit = $data1;
                                    $product->wholesale_price = $product->wholesale_price * $unit;
                                    $product->retail_price = $product->retail_price * $unit;
                                }
                            }
                        }
                                            ?>
                                            <tr class="gradeX">
                                                <td> 
                                                    <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" 
                                                         style="height: 30px; width: 50px;" class="thumb-img" alt="<?php echo $product->title; ?>">
                                                </td>
                                                <td><?php echo $product->title; ?></td>
                                                <td><?php echo $product->description; ?></td>
                                                                            <td><?php echo $unit; ?></td>
                                                <?php
                                                $exist = '';
                                                foreach ($offers as $offer) {
                                                    if ($product->id == $offer) {
                                                        $exist = 1;
//                                                        d($product->wholesale_price);
//                                                        d($total);
//                                                        $total = $total+$product->wholesale_price;
//                                                        d($total);
                                                        ?>
                                                        <?php
                                                    } else {
//                                                        $total += $product->retail_price;
                                                    }
                                                } if ($exist == '') {
                                                    $total = $total+$product->retail_price;
                                                    ?>
                                                    <td><?php echo $product->retail_price; ?></td>
                                                <?php } else{ 
                                                    $total = $total+$product->wholesale_price;
                                                    ?>
    <td><del><?php echo $product->retail_price; ?></del> <?php echo $product->wholesale_price; ?></td>
                                                <?php } ?>

                                            </tr>
                                            <?php
                                        }
                                    }
//                                    dd('end');
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row" style="border-radius: 0px">
                    <div class="col-md-3 col-md-offset-9">
<!--                        <p class="text-right"><b>Sub-total:</b> 2930.00</p>
                        <p class="text-right">Discout: 12.9%</p>
                        <p class="text-right">VAT: 12.9%</p>-->
                        <hr>
                        <h3 class="text-right">PKR : <?php echo $total; ?></h3>
                    </div>
                </div>
                <hr>
                <div class="hidden-print">
                    <div class="pull-right">
                        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                        <a href="<?php echo AppInterface::createURL(['site/dashboard']); ?>" class="btn btn-primary waves-effect waves-light">Dashboard</a>
                    </div>
                </div>
                <strong>Note : </strong>Your order is send successfully, and you will get your order in 3 working days!
            </div>
        </div>

    </div>

</div>
