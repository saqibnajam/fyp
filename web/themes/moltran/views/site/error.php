<?php
/* @var $this SiteController */
/* @var $error array */
//dd($exception);
?>

    <div class="wrapper-page">
            <div class="ex-page-content text-center">
                <h1><?php echo $exception->statusCode; ?>!</h1>
                <h2><?php echo $message; ?></h2><br>
                <br>
                <a class="btn btn-purple waves-effect waves-light" href="<?php echo Yii::$app->urlManager->baseUrl.'/site/dashboard';?>"><i class="fa fa-angle-left"></i> Back to Dashboard</a>
            </div>
        </div>