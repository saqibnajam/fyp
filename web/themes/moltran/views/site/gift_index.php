<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Gift List'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <?php
        echo $this->render('//shared/add_button', array('action' => 'product/main/add'));
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Product</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($model as $data) {
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data->title)) ?>
                        <?php echo Html::tag('td', Html::encode($data->product->title)) ?>
                        <?php echo Html::tag('td', Html::encode($data->description)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['site/giftdelete', 'id' => $data->id]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o"> delete</i></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
