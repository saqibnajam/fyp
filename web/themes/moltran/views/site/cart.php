<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\offers\models\Offers;
use app\modules\business\models\BusinessAds;
use app\modules\product\models\Product;
use app\components\AppInterface;
use app\modules\user\components\AppUser;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Shopping Cart'));
?>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="m-b-30">
                    <a href="<?php echo AppInterface::createURL(['site/dashboard']); ?>">
                        <button class="btn btn-primary waves-effect waves-light">Add <i class="fa fa-plus"></i></button>
                    </a>
                </div>
            </div>
        </div>

        <table class="table" id="_datatable-editable">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Unit</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $total = '';
                if (isset($order) && $order != '') {
//                    foreach (Yii::$app->session['unit'] as $key1 => $data1) {
                    foreach ($order as $key => $data) {
                        $product = Product::find()->where(['is_deleted' => 0, 'id' => $data])->one();
                        $unit = 1;
                        if (isset(Yii::$app->session['unit']) && Yii::$app->session['unit'] != '') {
                            foreach (Yii::$app->session['unit'] as $key1 => $data1) {
                                if ($key1 == $product->id) {
                                    $unit = $data1;
                                    $product->wholesale_price = $product->wholesale_price * $unit;
                                    $product->retail_price = $product->retail_price * $unit;
                                }
                            }
                        }
                        ?>
                    <form action="cart" method="post">        
                        <tr class="gradeX">
                            <td> 
                                <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" 
                                     style="height: 30px; width: 50px;" class="thumb-img" alt="<?php echo $product->title; ?>">
                            </td>
                            <td><?php echo $product->title; ?></td>
                            <td><?php echo $product->description; ?></td>
                            <td><input style="max-width:50px;" type="text" name="unit" value="<?php echo $unit; ?>"></td>
                        <input type="text" name="product" value="<?php echo $data; ?>" style="display : none;">
                        <?php
                        $exist = '';
                        foreach ($offers as $offer) {
                            if ($product->id == $offer) {
                                $exist = 1;
                                $total += $product->wholesale_price;
                                ?>
                                <td><del><?php echo $product->retail_price; ?></del> <?php echo $product->wholesale_price; ?></td>
                                <?php
                            } else {
                                $total += $product->retail_price;
                            }
                        } if ($exist == '') {
                            ?>
                            <td><?php echo $product->retail_price; ?></td>
                        <?php } ?>
                        <td class="actions">
                            <!--<a href="<?php // echo AppInterface::createURL(['site/cart', 'order' => $data]);    ?>"--> 
                               <!--class="on-default"><i class="fa fa-edit">Update</i>-->
                            <input type="submit" value="update">
                            <!--</a>-->
                            <a href="<?php echo AppInterface::createURL(['site/cart', 'id' => $key]); ?>" 
                               class="on-default"><i class="fa fa-trash-o">delete</i>
                            </a>
                        </td>
                        </tr>
                    </form>
                    <?php // ActiveForm::end(); ?>     
                    <?php
                }
//                }
            }
            ?>

            </tbody>
            <tfoot>
                <tr>
                    <td></td>                    
                    <td></td>                    
                    <td></td>                    
                    <td><b>Total</b></td>                    
                    <td><b><?php echo $total; ?></b></td>                    
                    <td class="actions">
                        <a href="<?php echo AppInterface::createURL(['site/checkout']); ?>" > 
                            <button class="btn-danger"><i class="fa fa-check-square-o">checkout</i></button>
                        </a>
                    </td>


                </tr>
            </tfoot>
        </table>
    </div>
    <!-- end: page -->

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&subset=latin-ext" rel="stylesheet">



    <!--Item slider text-->
    <div class="container">
        <div class="row" id="slider-text">
            <div class="col-md-6" >
                <h2>Recommendations</h2>
            </div>
        </div>
    </div>

    <!-- Item slider-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                    <div class="carousel-inner">

                        <?php
                        if (isset($recommendations) && $recommendations != '') {
                            foreach ($recommendations as $key => $product) {
                                ?>
                                <div class="item <?php echo $key == 0 ? 'active' : ''; ?>">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <a href="#"><img style="height: 100px; width: 150px;" src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" class="img-responsive center-block"></a>
                                        <h3 class="text-center"><?php echo $product->title; ?></h3>
                                        <h4 class="text-center">Rs: <?php echo $product->retail_price; ?></h4>
                                        <div class="text-center">
                                            <button style="width: 150px;" type="button" onclick="addCart(<?php echo $product->id; ?>)" class="btn btn-block btn-primary waves-effect waves-light"><i class="fa fa-cart-plus"></i> <span> ADD TO CART</span></button>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div id="slider-control">
                        <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive"></a>
                        <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- Item slider end-->

    <script>
        $(document).ready(function () {

            $('#itemslider').carousel({interval: 3000});

            $('.carousel-showmanymoveone .item').each(function () {
                var itemToClone = $(this);

                for (var i = 1; i < 6; i++) {
                    itemToClone = itemToClone.next();

                    if (!itemToClone.length) {
                        itemToClone = $(this).siblings(':first');
                    }

                    itemToClone.children(':first-child').clone()
                            .addClass("cloneditem-" + (i))
                            .appendTo($(this));
                }
            });
        });

    </script>

    <script>
        function addCart(id) {
            var url = "" + "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/site/addtocart";
            $.ajax({url: url,
                data: {id: id},
                type: 'GET',
                success: function (result) {
                    console.log(result);
                    alert('Successfully add to cart.');
                    location.reload();
<?php // \Yii::$app->getSession()->setFlash('success','Successfully add to cart.');         ?>

                },
                error: function () {
                    alert('Error occured');
                }
            });
        }
    </script>