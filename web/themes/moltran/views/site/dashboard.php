<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\components\AppUser;

/* @var $this yii\web\View */
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php
echo $this->render('_title', array('type' => 'DASHBOARD'));
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="portfolioFilter">
            <a class="btn-lg m-b-5" href="#" data-filter="*" class="current">All</a>
            <?php
            foreach ($types as $type) {
                ?>
                <a class="btn-lg m-b-5" href="#" data-filter=".<?php echo $type->id; ?>"><?php echo $type->title; ?></a>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row port">
    <div class="portfolioContainer">

        <?php
        if ($products != '' && $products != NULL) {
            foreach ($products as $product) {
                ?>
                <div class="col-sm-6 col-lg-3 col-md-2 <?php echo $product->type_id; ?>">
                    <div class="gal-detail thumb">
                        <a href="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" 
                           class="image-popup" title="<?php echo $product->title; ?>">
                            <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" 
                                 style="height: 150px; width: 230px;" class="thumb-img" alt="work-thumbnail">
                        </a>
                        <h4 style="color:#317eeb"><?php echo $product->title; ?></h4>
                        <p><?php echo strlen($product->description) > 30 ? substr($product->description, 0, 30) . '...' : $product->description; ?></p>
                        <div class="panel panel-default">
                            <?php if ((!isset(AppUser::getCurrentUser()->type)) || (AppUser::getCurrentUser()->type == 'customer')) { ?>
                                <div class="panel-heading">
                                    <button type="button" onclick="addCart(<?php echo $product->id; ?>)" class="btn btn-block btn-primary waves-effect waves-light"><i class="fa fa-cart-plus"></i> <span>ADD TO CART</span></button>
                                </div>
                            <?php } ?>
                            <?php
                            $exist = '';
                            foreach ($offers as $offer) {
                                if ($product->id == $offer) {
                                    $exist = 1;
                                    ?>
                                    <h5 class="red" style="text-align: center;">Rs: <del><?php echo $product->retail_price; ?></del> <?php echo $product->wholesale_price; ?> - <span style="color: green; font-size: larger;">In Offer</span></h5>
                                    <?php
                                }
                            } if ($exist == '') {
                                ?>
                                <h5 class="red" style="text-align: center;">Rs: <?php echo $product->retail_price; ?></h5>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div>
                <br>
                <br>
                No product found..            
            </div>

        <?php } ?>
    </div>
</div> <!-- End row -->

<script>
    function addCart(id) {
        var url = "" + "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/site/addtocart";
        $.ajax({url: url,
            data: {id: id},
            type: 'GET',
            success: function (result) {
                console.log(result);
                alert('Successfully add to cart.');
                location.reload();
<?php // \Yii::$app->getSession()->setFlash('success','Successfully add to cart.');           ?>

            },
            error: function () {
                alert('Error occured');
            }
        });
    }
</script>
<script type="text/javascript">
    $(window).load(function () {
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function () {
        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    });
</script>