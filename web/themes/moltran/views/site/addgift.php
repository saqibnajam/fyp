<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<?php
echo $this->render('_title', array('type' => 'Add Gifts'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateProduct();', 'id' => 'buss_form']]); ?>
                    <label class="col-md-2 control-label">Title</label>
                    <div class="col-md-9">
                        <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                    </div>
                    <div class="form-group field-producttype-title required">
                        <label class="col-sm-2 control-label">Product</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="Gifts[product_id]">
                                <option value="" >Select Product</option>
                                <?php foreach ($products as $type) { ?>
                                    <option value="<?php echo $type->id; ?>" > <?php echo $type->title; ?></option>
                                <?php } ?>
                            </select>
                            <!--<div class="help-block red"><?php // echo isset($model->errors['type_id']) && $model->errors['type_id'] != '' ? 'Product Type should be selected' : ''; ?></div>-->
                        </div>                    
                    </div>                    
                    <label class="col-md-2 control-label">Description</label>
                    <div class="col-md-9">
                        <?php echo $form->field($model, 'description')->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))->label(false); ?>
                    </div>

                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                        <?php
                        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                        ?>
                    </div>
<?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>