<?php
/* @var $this SiteController */
/* @var $error array */
$message = Yii::app()->user->getFlash('error');
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div style="width:97%">
    <div class="page-inner">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/earth.jpg" class="img-responsive" alt=""> 
    </div>
<div class="container error-404">
            <h1>404</h1>
            <h2>Houston, we have a problem.</h2>
            <p> Actually, the page you are looking for does not exist. </p>
        </div>
    <div class="form-actions">
            <div class="row" style="text-align:center;">
                    <a href="<?php echo Yii::app()->createUrl('/site/index');?>" class="btn btn-primary">Return home</a>
                    <a href="javascript:parent.history.back();" class="btn btn-default" style="margin-left:20px;">Go back</a>
            </div>
    </div>
</div>