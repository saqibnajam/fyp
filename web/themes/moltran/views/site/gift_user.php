<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'Lucky Customer'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <?php
        echo $this->render('//shared/add_button', array('action' => 'site/luckydraws'));
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Customer</th>
                    <th>Product</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($model as $data) {
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data->user->f_name.' '.$data->user->l_name)) ?>
                        <?php echo Html::tag('td', Html::encode($data->product->title)) ?>
                        <?php echo Html::tag('td', Html::encode(date('d-M-Y', $data->created_at))) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['site/usergiftdelete', 'id' => $data->id]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o"> delete</i></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
