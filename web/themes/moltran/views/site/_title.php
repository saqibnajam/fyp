<?php

use app\components\AppInterface;
use yii\widgets\Block;

/* @var $this yii\web\View */
?>

<!-- Page-Title -->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_-->
<?php
// $this->endBlock(); 
?>

<!-- Page-Header -->
<div class="row">
    <div class="col-sm-12">
        <h3 class="pull-left page-title"><?php echo $type; ?></h3>
        <ol class="breadcrumb pull-right">
            <li>
                <a href="<?php echo AppInterface::createURL('site/dashboard'); ?>">
                    <?php echo AppInterface::getAppName(); ?> 
                </a>
            </li>
            <li class="active"><?php echo $type; ?></li>
        </ol>
    </div>
</div>