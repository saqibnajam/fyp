<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;

//use yii\widgets\Block;
?>

<html>
    <head>
        <?php echo $this->render('//shared/head_meta'); ?>
    </head>

    <body class="fixed-left">
        <div id="wrapper">
            <?php echo $this->render('//shared/h_dashboard'); ?>
            <div class="content">
                <div class="container"> 
                    <br><br><br><br>
                    <div class="col-lg-12">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <?php echo $this->render('//shared/flashmessage'); ?>
                            <?php echo $content; ?>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>  
            </div> 
            <br>
            <?php echo $this->render('//shared/footer'); ?>
        </div>

    </body>

</html>