<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;
//use yii\widgets\Block;
?>

<html>
    <head>
        <?php echo $this->render('//shared/head_meta'); ?>
    </head>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
             <!---*** Remove this and use AppInterface Method to get the path  -->
            <?php 
            echo $this->render('//shared/header'); ?>
              <!---***-->
            <?php echo $this->render('//shared/menu'); ?>
          
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container"> 
                        <?php echo $this->render('//shared/flashmessage'); ?>

                        <?php echo $content; ?>
                    </div> <!-- container -->
                </div> <!-- content -->
                <?php echo $this->render('//shared/footer'); ?>
            </div>
        </div>

    </body>

</html>