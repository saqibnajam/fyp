<!DOCTYPE html>
<?php

use yii\base\Controller;
use app\components\AppInterface;
?>

<html>
    <head>

        <?php echo $this->render('//shared/head_auth'); ?>

        <?php
        if ($this->blocks['page_title'] != NULL && !empty($this->blocks['page_title'])) {
            $title = $this->blocks['page_title'];
        } elseif (Yii::$app->controller->id == 'site') {
            $title = Yii::$app->controller->module->requestedAction->id;
        } else {
            $title = Yii::$app->controller->module->module->requestedAction->id;
        }
        ?>
        <title>
            <?php echo AppInterface::getAppName(); ?> -
            <?php echo $title; ?>
        </title>

    </head>
    <body>
        <div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">

                <?php
                if (Yii::$app->session->hasFlash('success') || Yii::$app->session->hasFlash('error')) {
                    echo $this->render('//shared/flashmessage');
                }
                ?>
                <?php echo $content; ?>
                <!--flash Message-->
                <script> $(".flashMessage").delay(4200).fadeOut(400);</script>


            </div>
        </div>
    </body>
</html>