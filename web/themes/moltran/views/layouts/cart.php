<!DOCTYPE html>
<?php

use yii\base\Controller;

//use app\components\AppInterface;
//use yii\widgets\Block;
?>

<html>
    <head>
        <?php // echo $this->render('//shared/head_meta'); ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <?php

        use app\components\AppInterface;

if ($this->blocks['page_title'] != NULL && !empty($this->blocks['page_title'])) {
            $title = $this->blocks['page_title'];
        } elseif (Yii::$app->controller->id == 'site') {
            $title = Yii::$app->controller->module->requestedAction->id;
        } else {
            $title = Yii::$app->controller->module->module->requestedAction->id;
        }
        ?>
        <title>
            <?php echo AppInterface::getAppName(); ?> -
            <?php echo $title; ?>
        </title>


        <?php
        $url = AppInterface::getBaseUrl();
        ?>

        <!-- Plugin Css-->
        <link rel="stylesheet" href="<?php echo $url; ?>/assets/plugins/magnific-popup/dist/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo $url; ?>/assets/plugins/jquery-datatables-editable/datatables.css">

        <link href="<?php echo $url; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url; ?>/assets/css/custom.css" rel="stylesheet" type="text/css">
        <script src="<?php echo $url; ?>/assets/js/modernizr.min.js"></script>

    </head>

    <body class="fixed-left">
        <div id="wrapper">
            <?php echo $this->render('//shared/h_dashboard'); ?>
            <div class="content">
                <div class="container"> 
                    <br><br><br><br>
                    <?php echo $content; ?>
                </div>  
            </div> 
            <br>
            <footer class="footer text-right">
                <?php echo Yii::$app->params['footer_txt']; ?>
            </footer>

            <script>
                var resizefunc = [];
            </script>
<script src="<?php echo $url; ?>/assets/js/custom.js"></script>
            <!-- jQuery  -->
            <script src="<?php echo $url; ?>/assets/js/jquery.min.js"></script>
            <script src="<?php echo $url; ?>/assets/js/bootstrap.min.js"></script>
            <script src="<?php echo $url; ?>/assets/js/detect.js"></script>
            <script src="<?php echo $url; ?>/assets/js/fastclick.js"></script>
            <script src="<?php echo $url; ?>/assets/js/jquery.slimscroll.js"></script>
            <script src="<?php echo $url; ?>/assets/js/jquery.blockUI.js"></script>
            <script src="<?php echo $url; ?>/assets/js/waves.js"></script>
            <script src="<?php echo $url; ?>/assets/js/wow.min.js"></script>
            <script src="<?php echo $url; ?>/assets/js/jquery.nicescroll.js"></script>
            <script src="<?php echo $url; ?>/assets/js/jquery.scrollTo.min.js"></script>

            <script src="<?php echo $url; ?>/assets/js/jquery.app.js"></script>

            <!-- Examples -->
            <script src="<?php echo $url; ?>/assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo $url; ?>/assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script> 
            <script src="<?php echo $url; ?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
            <script src="<?php echo $url; ?>/assets/pages/datatables.editable.init.js"></script>

            <?php // echo $this->render('//shared/footer'); ?>
        </div>

    </body>

</html>