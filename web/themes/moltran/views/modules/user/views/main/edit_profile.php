<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<?php // dd($model->f_name); ?>
<div class="row">
    <div class="col-sm-12">

        <!---*** should not add inline-css --->
        <div class="bg-picture text-center bg-image">
            <div class="bg-picture-overlay"></div>
            <div class="profile-info-name">
                <img src="<?php echo AppInterface::getUserImage(); ?>" 
                     class="thumb-lg img-circle img-thumbnail" alt="profile-image">
                <h3 class="text-white"><?php echo \Yii::$app->user->identity->f_name; ?> <?php echo \Yii::$app->user->identity->l_name; ?></h3>
                <h4 class="text-white"><?php // echo \Yii::$app->user->identity->userRoles[0]->role->title; ?></h4>
            </div>
        </div>
        <!--/ meta -->
    </div>
</div>

<div class="row user-tabs" style="margin: 0 auto;">
    <div class="col-lg-6 col-md-9 col-sm-9">
        <ul class="nav nav-tabs tabs">
            <li class="active tab">
                <a href="#home-2" data-toggle="tab" aria-expanded="false" class="active"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">About Me</span> 
                </a> 
            </li> 
            <li class="tab"> 
                <a href="#settings-2" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                    <span class="hidden-xs">Edit Profile</span> 
                </a> 
            </li> 
            <div class="indicator"></div>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12"> 

        <div class="tab-content profile-tab-content"> 
            <div class="tab-pane active" id="home-2"> 
                <div class="row">
                    <div class="col-md-6">
                        <!--<a href="<?php // echo AppInterface::createURL('user/main/password')?>" class="btn-primary btn-block btn waves-effect waves-light">Change Password</a>-->
                        <!-- Personal-Information -->
                        <div class="panel panel-default panel-fill">
                            <div class="panel-heading"> 
                                <h3 class="panel-title">Personal Information</h3> 
                            </div> 
                            <div class="panel-body"> 
                                <div class="about-info-p">
                                    <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $model->f_name ?> <?php echo $model->l_name ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted"><?php echo isset($model->phone) ? $model->phone : '-' ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Email</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $model->email; ?></p>
                                </div>
<!--                                <div class="about-info-p m-b-0">
                                    <strong>Country</strong>
                                    <br>
                                    <p class="text-muted"><?php // echo $model->country->title ?></p>
                                </div>-->
                            </div> 
                        </div>
                        <!-- Personal-Information -->

                    </div>

                    <?php if($model->description != ''){ ?>
                    <div class="col-md-6">
                        <div class="panel panel-default panel-fill">
                            <div class="panel-heading"> 
                                <h3 class="panel-title">Description</h3> 
                            </div> 
                            <div class="panel-body"> 
                                <p><?php echo $model->description ?></p>
                            </div> 
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div> 

            <div class="tab-pane" id="settings-2">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                echo $this->render('_form', array
                                    ('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
