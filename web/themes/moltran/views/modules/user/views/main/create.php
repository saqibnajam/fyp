<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

echo $this->render('_title', array('type' => 'user'));
if ($model->errors) {
    echo $model->errors;    ///*** Put this code inside the form and render it 
    //properly with error containers. This is an array so handle it properly
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-horizontal" >
                    <!---*** There should be only one form validation, checkout validation rules  -->
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateUser();', 'id' => 'user_form']]); ?>
                    <label class="col-md-2 control-label">First Name</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'f_name')
                                ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true))->label(false);
                        ?>
                    </div>

                    <label class="col-md-2 control-label">Last Name</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'l_name')
                                ->textInput(array('placeholder' => 'Last Name'))->label(false);
                        ?>
                    </div>

                    <label class="col-md-2 control-label">Email</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
                        ?>
                    </div>

                    <?php if ($model->isNewRecord) { ?>
                        <label class="col-md-2 control-label">Password</label>
                        <div class="col-md-9">
                            <?php echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                        </div>
                    <?php } ?>

                    <label class="col-md-2 control-label">Date of Birth</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'dob', ['inputOptions' => ['value' => $model->dob != '' ? date('d-M-Y', $model->dob) : '']])->textInput(
                                array('placeholder' => 'Date Of Birth', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control col-md-12 dob'))->label(false);
                        ?>
                    </div>

                    <label class="col-md-2 control-label">Phone</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'phone')->textInput(
                                        array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                                ->label(false);
                        ?>
                    </div>

                    <label class="col-md-2 control-label">Address</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'address')->textInput(
                                        array('placeholder' => 'Address'))
                                ->label(false);
                        ?>                            
                    </div>

                    <label class="col-md-2 control-label">Description</label>
                    <div class="col-md-9">
                        <?php
                        echo $form
                                ->field($model, 'description')
                                ->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))
                                ->label(false);
                        ?>
                    </div>
                    <!---*** Use YII 2.0 select field  --->
                    <div class="form-group field-businesstype-title required">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="User[country_id]" id="country">
                                <option value="0">Select Country</option>
                                <?php foreach ($countries as $country) { ?>
                                    <option <?php echo isset($model->country_id) && $model->country_id == $country->id ? 'selected' : ''; ?> value="<?php echo $country->id; ?>" > <?php echo $country->title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <!---*** --->
                    <label class="col-md-2 control-label">State</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'state')->textInput(
                                        array('placeholder' => 'County'))
                                ->label(false);
                        ?>                            
                    </div>

                    <label class="col-md-2 control-label">City</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'city')->textInput(
                                        array('placeholder' => 'City'))
                                ->label(false);
                        ?>                            
                    </div>


                    <label class="col-md-2 control-label">Postal Code</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'postal_code')->textInput(
                                array('placeholder' => 'Postal Code', 'required' => 'required', 'aria-required' => true))->label(false);
                        ?>                            
                    </div>
                    <?php if (isset($model->image) && $model->image != '') { ?>
                        <label class="col-md-2 control-label">Current Logo</label>
                        <div class="col-md-9">
                            <img src="<?php echo $model->image; ?>" 
                                 class="img-thumbnail" style="height: 80px; width: 80px;">
                        </div>
                    <?php } ?>
                    <label class="col-md-2 control-label">Upload Image</label>
                    <div class="col-md-9">
                        <?php
                        echo $form->field($model, 'image')->fileInput()->label(false);
                        ?>
                    </div>
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                        <?php
                        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                        ?>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>
