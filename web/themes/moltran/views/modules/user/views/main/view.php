<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\User;
use app\modules\user\models\Role;
use app\models\Country;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'view', 'index' => 'user/main/index'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">First Name :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->f_name) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Last Name</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->l_name) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->email) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Phone</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->phone) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Address</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->address) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">State :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->state_id) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">City</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->city_id) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->country_id) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Description :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7"> 
                            <?php echo Html::encode($model->description); ?>
                        </div>    
                    </div>    
<!--                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Image</label>
                            <div class="col-md-1"></div>
                            <div class="col-md-7">
                                <?php // if (isset($model->image)) { ?>
                                    <img src="<?php // echo $model->image; ?>" 
                                         class="img-thumbnail" style="height: 80px;">
                                     <?php // } ?>
                            </div>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-2">
                            <a href="<?php echo AppInterface::createURL('user/main/index'); ?>"> 
                                <?php
                                echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                                ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>