<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!--use for page title-->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_asdf-->
<?php // $this->endBlock(); ?>
<div class="panel-heading bg-img"> 
    <div class="bg-overlay"></div>
    <h3 class="text-center m-t-10 text-white"> Sign In to <strong><?php echo AppInterface::getAppName(); ?></strong> </h3>
</div> 


<div class="panel-body">
    <div class="form-horizontal m-t-20" >
        <?php $form = ActiveForm::begin(); ?>

        <div class="field-businesstype-title required">        
            <?php echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'class' => 'form-control input-lg', 'required' => 'required'))->label(false); ?>
        </div>

        <div class="col-xs-12 text-center">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg w-lg waves-effect waves-light']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>                                 

