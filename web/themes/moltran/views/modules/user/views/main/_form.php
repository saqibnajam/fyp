
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<div class="form-horizontal" >
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateCustomer();', 'id' => 'customer_form']]); ?>
    <label class="col-md-2 control-label">First Name</label>                
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'f_name')
                ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>
    </div>
    <label class="col-md-2 control-label">Last Name</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'l_name')
                ->textInput(array('placeholder' => 'Last Name'))->label(false);
        ?>
    </div>
    <label class="col-md-2 control-label">Email</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
        ?>
    </div>
    <?php if ($model->isNewRecord) { ?>
        <label class="col-md-2 control-label">Password</label>
        <div class="col-md-9">
            <?php echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'required' => 'required', 'aria-required' => true))->label(false); ?>
        </div>
    <?php } ?>
    <label class="col-md-2 control-label">Phone</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'phone')->textInput(
                        array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                ->label(false);
        ?>
    </div>
    <label class="col-md-2 control-label">Address</label>
    <div class="col-md-9">
        <?php
        echo $form->field($model, 'address')->textInput(
                        array('placeholder' => 'Address'))
                ->label(false);
        ?>                            
    </div>

    <div class="form-group" >
        <label class="col-md-2 control-label">Country</label>
        <div class="col-md-9">
            <select class="form-control" name="User[country_id]" id="country_id" onchange="getstate(this.value)">
                <option value="" >Select Any Country</option>
                <?php foreach ($countries as $data) { ?>
                    <option <?php echo isset($model->country_id) && $model->country_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="form-group" >
        <label class="col-md-2 control-label">State</label>
        <div class="col-md-9">
            <select class="form-control" name="User[state_id]" id="state" onchange="getcity(this.value)">
                <option value="" >Select Any State</option>
                <?php foreach ($states as $data) { ?>
                    <option <?php echo isset($model->state_id) && $model->state_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                <?php } ?>

            </select>
        </div>
    </div>

    <div class="form-group" >
        <label class="col-md-2 control-label">City</label>
        <div class="col-md-9">
            <select class="form-control" name="User[city_id]" id="city">
                <option value="" >Select Any City</option>
                <?php foreach ($cities as $data) { ?>
                    <option <?php echo isset($model->city_id) && $model->city_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                <?php } ?>

            </select>
        </div>
    </div>
    <label class="col-md-2 control-label">Description</label>
    <div class="col-md-9">
        <?php
        echo $form
                ->field($model, 'description')
                ->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))
                ->label(false);
        ?>
    </div>

    <label class="col-md-2 control-label"></label>
    <div class="col-md-2">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>     
</div>

<script>
    function getstate(id) {
        var url = "" + "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/user/main/getstate?name=User[state_id]";
        $.ajax({url: url,
            data: {country_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $('#state').html(result);
<?php if (isset($model->state_id)) { ?>
                    $("#state").val('<?php echo $model->state_id; ?>');
<?php } ?>
            },
            error: function () {
                alert('Error occured');
            }
        });
    }
    function getcity(id) {
        var url = "<?php echo Yii::$app->urlManager->baseUrl; ?>" + "/user/main/getcity?name=User[city_id]";
        $.ajax({url: url,
            data: {state_id: id},
            type: 'POST',
            success: function (result) {
                console.log(result);
                $('#city').html(result);
<?php if (isset($model->city_id)) { ?>
                    $("#city").val('<?php echo $model->city_id; ?>');
<?php } ?>
            },
            error: function () {
                alert('Error occured');
            }
        });
    }

</script>