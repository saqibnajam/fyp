<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'index'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Date</th>
                    <th>Product</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($model as $data) {
                    ?>
                    <tr class="gradeC">
                        <td> 
                            <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $data->product->image; ?>" 
                                 style="height: 30px; width: 50px;" class="thumb-img" alt="<?php echo $data->product->title; ?>">
                        </td>
                        <?php echo Html::tag('td', Html::encode(date('d-M-Y', $data->created_at))) ?>
                        <?php echo Html::tag('td', Html::encode($data->product->title)) ?>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
