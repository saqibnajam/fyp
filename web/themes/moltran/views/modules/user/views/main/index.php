<?php

use yii\helpers\Html;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\Role;
use app\modules\business\models\Business;
use app\modules\user\components\AppUser;
use app\modules\privilege\components\PrivilegeComponent;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 's_list'));
?>
<!-- Page-body -->
<div class="panel">
    <div class="panel-body">
        <?php
        echo $this->render('//shared/add_button', array('action' => 'user/main/add'));
        ?>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Ph Number</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model as $data) {
                    $phone = isset($data->phone) ? $data->phone : '-';
                    $business = isset($data->business_id) && $data->business_id != '' ?
                            Business::findOne(['id' => $data->business_id])->title : '-';
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data->f_name . ' ' . $data->l_name)) ?>
                        <?php echo Html::tag('td', Html::encode($data->email)) ?>
                        <?php echo Html::tag('td', Html::encode($data->phone)) ?>
                        <?php echo Html::tag('td', Html::encode($data->description)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['user/main/view', 'id' => $data->id]); ?>" 
                                           class="on-default"><i class="ion ion-eye">view</i>
                                        </a></li>
                                    <li><a href="<?php echo AppInterface::createURL(['user/main/edit', 'id' => $data->id]); ?>" 
                                           class="on-default"><i class="fa fa-pencil">edit</i>
                                        </a></li>
                                    <li><a href="<?php echo AppInterface::createURL(['user/main/delete', 'id' => $data->id, 'type' => 'index']); ?>" 
                                           class="on-default"><i class="fa fa-trash-o">delete</i>
                                        </a></li>
                                </ul>
                            </div>
                        </td>  
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>
    </div>    <!-- end: page -->
</div> <!-- end Panel -->