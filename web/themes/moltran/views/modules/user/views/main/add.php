<?php
echo $this->render('_title', array('type' => 'add'));
if ($model->errors) {
    echo $model->errors;    ///*** Put this code inside the form and render it 
                            //properly with error containers. This is an array so handle it properly
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                echo $this->render('_form', array('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
                ?>
            </div>
        </div>
    </div>
</div>
