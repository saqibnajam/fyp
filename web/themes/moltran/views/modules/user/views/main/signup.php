<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!--use for page title-->
<?php // $this->beginBlock('page_title'); ?>
<!--Add_asdf-->
<?php // $this->endBlock(); ?>
<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
        <div class="panel-heading bg-img"> 
            <div class="bg-overlay"></div>
            <h3 class="text-center m-t-10 text-white"> Signup to <strong><?php echo AppInterface::getAppName(); ?></strong> </h3>
        </div> 


        <div class="panel-body">
            <div class="form-horizontal" >
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onSubmit' => 'return validateCustomer();', 'id' => 'customer_form']]); ?>
                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'f_name')
                            ->textInput(array('placeholder' => 'First Name', 'required' => 'required', 'aria-required' => true))->label(false);
                    ?>
                </div>

                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'l_name')
                            ->textInput(array('placeholder' => 'Last Name'))->label(false);
                    ?>
                </div>

                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'email')->input('email', array('placeholder' => 'Email', 'required' => 'required', 'aria-required' => true))->label(false);
                    ?>
                </div>

                <div class="col-md-12">
                    <?php echo $form->field($model, 'password')->input('password', array('placeholder' => 'Password', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                </div>

                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'phone')->textInput(
                                    array('placeholder' => 'Phone', 'onkeypress' => 'return isNumberKey(event);'))
                            ->label(false);
                    ?>
                </div>

                <div class="col-md-12">
                    <?php
                    echo $form->field($model, 'address')->textInput(
                                    array('placeholder' => 'Address'))
                            ->label(false);
                    ?>                            
                </div>

                <div class="form-group" >
                    <div class="col-md-12">
                        <select class="form-control" name="User[country_id]" id="country_id" onchange="getstate(this.value)">
                            <option value="" >Select Any Country</option>
                            <?php foreach ($countries as $data) { ?>
                                <option <?php echo isset($model->country_id) && $model->country_id == $data->id ? 'selected' : ''; ?> value="<?php echo $data->id; ?>" > <?php echo $data->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group" >
                    <div class="col-md-12">
                        <select class="form-control" name="User[state_id]" id="state" onchange="getcity(this.value)">
                            <option value="" >Select Any State</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" >
                    <div class="col-md-12">
                        <select class="form-control" name="User[city_id]" id="city">
                            <option value="" >Select Any City</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <?php
                    echo $form
                            ->field($model, 'description')
                            ->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))
                            ->label(false);
                    ?>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" checked="checked">
                            <label for="checkbox-signup">
                                I accept <a href="#">Terms and Conditions</a>
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-primary waves-effect waves-light btn-lg w-lg" type="submit">Register</button>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>     
            </div>
            <div class="form-group m-t-30">
                <div class="col-sm-12 text-center">
                    <a href="<?php echo AppInterface::createURL('user/main/login'); ?>">Already have account?</a>
                </div>
            </div>

        </div>
    </div>

    <script>
            function getstate(id) {
                var url = "" + "<?php echo \Yii::$app->getRequest()->baseUrl; ?>" + "/user/main/getstate?name=User[state_id]";
                $.ajax({url: url,
                    data: {country_id: id},
                    type: 'POST',
                    success: function (result) {
                        console.log(result);
                        $('#state').html(result);
<?php if (isset($model->state_id)) { ?>
                            $("#state").val('<?php echo $model->state_id; ?>');
<?php } ?>
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });
            }
            function getcity(id) {
                var url = "<?php echo Yii::$app->urlManager->baseUrl; ?>" + "/user/main/getcity?name=User[city_id]";
                $.ajax({url: url,
                    data: {state_id: id},
                    type: 'POST',
                    success: function (result) {
                        console.log(result);
                        $('#city').html(result);
<?php if (isset($model->city_id)) { ?>
                            $("#city").val('<?php echo $model->city_id; ?>');
<?php } ?>
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });
            }
       
    </script>