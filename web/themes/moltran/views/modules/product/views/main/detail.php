<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\user\models\UserRole;
use app\modules\user\models\User;
use app\modules\user\models\Role;
use app\models\Country;
use app\modules\product\models\Product;

//dd($model[0]['status']);
/* @var $this yii\web\View */
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Invoice</h4>
            </div> 
            <div class="panel-body">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-right"><img src="<?php echo AppInterface::getBaseUrl(); ?>/assets/images/shoppingcart2.png"
                                                    alt="<?php echo AppInterface::getAppName(); ?>" title="<?php echo AppInterface::getAppName(); ?>" style="height: 50px;width: 185px;"></h4>

                    </div>
                    <div class="pull-right">
                        <h4>Order # <br>
                            <strong><?php echo $model[0]['id']; ?></strong>
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left m-t-30 col-md-6">
                            <address>
                                <strong>Shipping Address: </strong><br>
                                <abbr title="Address">Address : </abbr> <?php echo $model[0]['address']; ?>
                                <br>
                                <abbr title="Phone">Phone # : </abbr> <?php echo $model[0]['phone']; ?>
                                <br>
                                <br>
                                <strong>Payment Details: </strong>
                                <br>
                                <?php
                                if ($model[0]['payment_type'] == 'card') {
                                    $method = 'Through Debite / Credit Card';
                                } else {
                                    $method = 'Cash on Delivery';
                                }
                                ?>                                
                                <abbr title="Method">Method : </abbr> <?php echo $method; ?>
                                <?php if ($model[0]['payment_type'] == 'card' && \app\modules\user\components\AppUser::getCurrentUser()->type != 'customer') { ?>
                                    <br><br>    
                                    <strong>Account Details: </strong>
                                    <br>
                                    <abbr title="Card Type">Card Type : </abbr> <?php echo $model[0]['card_type']; ?>
                                    <br><abbr title="Card Number">Card Number : </abbr> <?php echo $model[0]['card_number']; ?>
                                    <br><abbr title="Card Verification">Card Verification # : </abbr> <?php echo $model[0]['card_verification']; ?>
                                <?php } ?>
                            </address>
                        </div>
                        <div class="pull-right m-t-30">
                            <p><strong>Order Date: </strong> <?php echo date('d-M-Y', $model[0]['issue_date']); ?></p>
                            <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-pink"><?php echo $model[0]['status']; ?></span></p>
                            <p class="m-t-10"><strong>Order ID: </strong> #<?php echo $model[0]['order_id']; ?></p>
                            <p class="m-t-10"><strong>Shipping Charges: </strong> Free</p>
                        </div>
                    </div>
                </div>
                <div class="m-h-50"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table" id="_datatable-editable">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $total = '';
                                    if (isset($order) && $order != '') {
                                        foreach ($order as $key => $data) {
//                                            dd($data->product_id);
                                            $product = Product::find()->where(['is_deleted' => 0, 'id' => $data->product_id])->one();
//                                            $total += $product->retail_price;
                                            ?>
                                            <tr class="gradeX">
                                                <td> 
                                                    <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $product->image; ?>" 
                                                         style="height: 30px; width: 50px;" class="thumb-img" alt="<?php echo $product->title; ?>">
                                                </td>
                                                <td><?php echo $product->title; ?></td>
                                                <td><?php echo $product->description; ?></td>
                                                <?php
                                                $exist = '';
                                                foreach ($offers as $offer) {
                                                    if ($product->id == $offer) {
                                                        $exist = 1;
                                                        $total += $product->wholesale_price;
                                                        ?>
                                                        <td><del><?php echo $product->retail_price; ?></del> <?php echo $product->wholesale_price; ?></td>
                                                        <?php
                                                    } else {
                                                        $total += $product->retail_price;
                                                    }
                                                } if ($exist == '') {
                                                    ?>
                                                    <td><?php echo $product->retail_price; ?></td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row" style="border-radius: 0px">
                    <div class="col-md-3 col-md-offset-9">
<!--                        <p class="text-right"><b>Sub-total:</b> 2930.00</p>
                        <p class="text-right">Discout: 12.9%</p>
                        <p class="text-right">VAT: 12.9%</p>-->
                        <hr>
                        <h3 class="text-right">PKR : <?php echo $total; ?></h3>
                    </div>
                </div>
                <hr>
                <div class="hidden-print">
                    <div class="pull-right">
                        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                        <a href="<?php echo AppInterface::createURL('product/main/order'); ?>" class="btn btn-primary waves-effect waves-light">Back</a>
                    </div>
                </div>
                <strong>Note : </strong>Your order is send successfully, and you will get your order in 3 working days!
            </div>
        </div>

    </div>

</div>
