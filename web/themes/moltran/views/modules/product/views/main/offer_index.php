<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'index'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <?php
        echo $this->render('//shared/add_button', array('action' => 'product/main/addoffer'));
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($model as $data) {
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data->title)) ?>
                        <?php echo Html::tag('td', Html::encode($data->description)) ?>
                        <?php echo Html::tag('td', Html::encode($data->status)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/deleteoffer', 'id' => $data->id]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o"> delete</i></a>
                                    </li>
                                    <hr>
                                    Update Status:
                                    <?php if ($data->status == 'inactive') { ?>
                                        <li><a href="<?php echo AppInterface::createURL(['product/main/offerstatus', 'id' => $data->id, 'type' => 'active']); ?>" 
                                               class="on-default"><i class="fa fa-pencil"> Active</i></a>
                                        </li>
                                    <?php } else { ?>
                                        <li><a href="<?php echo AppInterface::createURL(['product/main/offerstatus', 'id' => $data->id, 'type' => 'inactive']); ?>" 
                                               class="on-default"><i class="fa fa-pencil"> In Active</i></a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
