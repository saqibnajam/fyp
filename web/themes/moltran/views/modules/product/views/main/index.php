<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'index'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <?php
        echo $this->render('//shared/add_button', array('action' => 'product/main/add'));
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Product Type</th>
                    <th>Buy Price</th>
                    <th>Retail Price</th>
                    <th>Discounted Price</th>
                    <!--<th>Status</th>-->
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $b_total = '';
                $r_total = '';
                $d_total = '';
                foreach ($model as $data) {
                    $b_total += $data->buy_price;
                    $r_total += $data->retail_price;
                    $d_total += $data->wholesale_price;
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data->title)) ?>
                        <?php echo Html::tag('td', Html::encode($data->type->title)) ?>
                        <?php echo Html::tag('td', Html::encode($data->buy_price)) ?>
                        <?php echo Html::tag('td', Html::encode($data->retail_price)) ?>
                        <?php echo Html::tag('td', Html::encode($data->wholesale_price)) ?>
                        <?php // echo Html::tag('td', Html::encode($data->status)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/view', 'id' => $data->id]); ?>" 
                                           class="on-default"><i class="ion ion-eye"> view</i></a></li>
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/edit', 'id' => $data->id]); ?>"
                                           class="on-default edit-row"><i class="fa fa-pencil"> edit</i></a></li>
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/delete', 'id' => $data->id]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o"> delete</i></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>                    
                    <td><b>Total</b></td>                    
                    <td><b><?php echo $b_total; ?></b></td>                    
                    <td><b><?php echo $r_total; ?></b></td>                    
                    <td><b><?php echo $d_total; ?></b></td>                    
                    <!--<td></td>-->                    
                    <td></td>                    


                </tr>
            </tfoot>

        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
