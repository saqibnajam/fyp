<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'view', 'index' => 'customer/main/index'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <!--<div class="panel-heading"><h3 class="panel-title">Form elements</h3></div>-->
            <div class="panel-body">
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Title :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->title) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Product Type :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->type->title) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Buy Price :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->buy_price) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Retail Price :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->retail_price) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Wholesale Price :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->wholesale_price) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Description :</label>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <?php echo Html::encode($model->description) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Upload Image :</label>
                            <div class="col-md-1"></div>
                            <div class="col-md-7">
                                <?php if ($model->image) { ?>
                                    <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $model->image; ?>" 
                                         class="img-thumbnail" style="height: 80px; width: 80px;">
                                         <?php
                                     } else {
                                         ?>
                                    <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('themes/moltran/assets/images/grocery-guru-logo.png'); ?>" 
                                         class="img-thumbnail" style="height: 80px; width: 80px;">
                                     <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-2">
                            <a href="<?php echo AppInterface::createURL('product/main/index'); ?>"> 
                                <?php
                                echo Html::submitButton('Back', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                                ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>