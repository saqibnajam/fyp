<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>

<div class="form-horizontal" >
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateProduct();', 'id' => 'buss_form']]); ?>
    <label class="col-md-2 control-label">Title</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
    </div>
    <div class="form-group field-producttype-title required">
        <label class="col-sm-2 control-label">Product Type</label>
        <div class="col-sm-9">
            <select class="form-control" name="Product[type_id]">
                <option value="" >Select Product Type</option>
                <?php foreach ($types as $type) { ?>
                    <option <?php echo $model->type_id == $type->id ? 'selected' : ''; ?> value="<?php echo $type->id; ?>" > <?php echo $type->title; ?></option>
                <?php } ?>
            </select>
            <div class="help-block red"><?php echo isset($model->errors['type_id']) && $model->errors['type_id'] != '' ? 'Product Type should be selected' : ''; ?></div>
        </div>                    
    </div>                    
    <label class="col-md-2 control-label">Buy Price</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'buy_price')->textInput(array('placeholder' => 'Buy Price', 'class' => 'form-control', 'required' => 'required', 'onkeypress' => 'return isNumberKey(event);', 'aria-required' => true))->label(false); ?>
    </div>
    <label class="col-md-2 control-label">Retail Price</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'retail_price')->textInput(array('placeholder' => 'Retail Price', 'class' => 'form-control', 'required' => 'required', 'onkeypress' => 'return isNumberKey(event);', 'aria-required' => true))->label(false); ?>
    </div>
    <label class="col-md-2 control-label">Discounted Price</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'wholesale_price')->textInput(array('placeholder' => 'Wholesale Price', 'class' => 'form-control', 'required' => 'required', 'onkeypress' => 'return isNumberKey(event);', 'aria-required' => true))->label(false); ?>
    </div>
    <label class="col-md-2 control-label">Description</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'description')->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))->label(false); ?>
    </div>

    <?php if (isset($model->image) && $model->image != '') { ?>
        <label class="col-md-2 control-label">Current Logo</label>
        <div class="col-md-9">
            <?php if ($model->image) { ?>
                <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('uploads/product/image') . '/' . $model->image; ?>" 
                     class="img-thumbnail" style="height: 80px; width: 80px;">
                     <?php
                 } else {
                     ?>
                <img src="<?php echo \Yii::$app->urlManager->createAbsoluteUrl('themes/moltran/assets/images/grocery-guru-logo.png'); ?>" 
                     class="img-thumbnail" style="height: 80px; width: 80px;">
                 <?php } ?>
        </div>
    <?php } ?>

    <label class="col-md-2 control-label">Upload Image</label>
    <div class="col-md-9">
        <?= $form->field($model, 'image')->fileInput()->label(false); ?>
    </div>
    <?php if (isset($model->image) && $model->image != '') { 
//     dd('here');
        ?>
        <input type="hidden" name="Product[image]" value="<?php echo $model->image; ?>">
    <?php } ?>
    <label class="col-md-2 control-label"></label>
    <div class="col-md-2">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>     
</div>