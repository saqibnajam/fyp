<?php
/* @var $this yii\web\View */
?>
<?php
echo $this->render('_title', array('type' => 'add'));
if ($model->errors) {
//    dd($model->errors['type_id']);
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                echo $this->render('_offer_form', array('model' => $model, 'products' => $products));
                ?>
            </div>
        </div>
    </div>
</div>