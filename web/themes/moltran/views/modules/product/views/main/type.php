<?php
/* @var $this yii\web\View */
?>
<?php
echo $this->render('_title', array('type' => 'add'));
if ($model->errors) {
//    dd($model->errors['type_id']);
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php

                use yii\helpers\Html;
                use yii\widgets\ActiveForm;
                use app\components\AppInterface;
                ?>

                <div class="form-horizontal" >
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateProduct();', 'id' => 'buss_form']]); ?>
                    <label class="col-md-2 control-label">Title</label>
                    <div class="col-md-9">
                        <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
                    </div>
                    <label class="col-md-2 control-label">Description</label>
                    <div class="col-md-9">
                        <?php echo $form->field($model, 'description')->textarea(array('placeholder' => 'Description', 'class' => 'form-control'))->label(false); ?>
                    </div>
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                        <?php
                        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
                        ?>
                    </div>
                    <?php ActiveForm::end(); ?>     
                </div>
            </div>
        </div>
    </div>
</div>