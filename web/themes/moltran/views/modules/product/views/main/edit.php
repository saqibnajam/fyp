<?php
echo $this->render('_title', array('type' => 'edit'));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
//                dd($model);
                echo $this->render('_form', array('model' => $model, 'types' => $types));
                ?>
            </div>
        </div>
    </div>
</div>