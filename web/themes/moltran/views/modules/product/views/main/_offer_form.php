<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
?>

<div class="form-horizontal" >
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return validateProduct();', 'id' => 'buss_form']]); ?>
    <label class="col-md-2 control-label">Title</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'title')->textInput(array('placeholder' => 'Title', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
    </div>
    <div class="form-group field-producttype-title required">
        <label class="col-sm-2 control-label">Add Products</label>
        <div class="col-sm-9">
            <select class="form-control" multiple name="Offers[product][]">
                <?php foreach ($products as $type) { ?>
                    <option value="<?php echo $type->id; ?>" > <?php echo $type->title; ?></option>
                <?php } ?>
            </select>
            <p class="help-block red">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.</p>
            <!--<div class="help-block red"><?php // echo isset($model->errors['type_id']) && $model->errors['type_id'] != '' ? 'Product Type should be selected' : ''; ?></div>-->
        </div>                    
    </div>                    
    <label class="col-md-2 control-label">Description</label>
    <div class="col-md-9">
        <?php echo $form->field($model, 'description')->textInput(array('placeholder' => 'Description', 'class' => 'form-control', 'required' => 'required', 'aria-required' => true))->label(false); ?>
    </div>
    <label class="col-md-2 control-label"></label>
    <div class="col-md-2">
        <?php
        echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn-primary btn-block btn waves-effect waves-light'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>     
</div>