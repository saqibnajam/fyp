<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\privilege\components\PrivilegeComponent

/* @var $this yii\web\View */
?>
<!-- Page-Title -->
<?php
echo $this->render('_title', array('type' => 'order'));
?>
<!-- Page-Body -->
<div class="panel">
    <div class="panel-body">
        <?php
//        echo $this->render('//shared/add_button', array('action' => 'product/main/add'));
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Order #</th>
                    <th>Customer</th>
                    <th>Order Date</th>
                    <th>Delivery Date</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($model as $data) {
                    $customer = \app\modules\user\models\User::findOne($data['customer_id']);
                    ?>
                    <tr class="gradeC">
                        <?php echo Html::tag('td', Html::encode($data['order_id'])) ?>
                        <?php echo Html::tag('td', Html::encode($customer['f_name'].' '.$customer['l_name'])) ?>
                        <?php echo Html::tag('td', Html::encode(date('d-M-Y', $data['issue_date']))) ?>
                        <?php echo Html::tag('td', Html::encode(date('d-M-Y', $data['delivery_date']))) ?>
                        <?php echo Html::tag('td', Html::encode($data['description'])) ?>
                        <?php echo Html::tag('td', Html::encode($data->status)) ?>
                        <td class="actions">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Actions <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/orderdetail', 'id' => $data['order_id']]); ?>" 
                                           class="on-default"><i class="ion ion-eye"> View Invoice</i></a></li>
                                    <hr>
                                    Update Status: 
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/updatestatus', 'id' => $data['order_id'], 'status' => 'confirm']); ?>"
                                           class="on-default edit-row"><i class="fa fa-pencil"> Confirm</i></a></li>
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/updatestatus', 'id' => $data['order_id'], 'status' => 'dispatched']); ?>"
                                           class="on-default edit-row"><i class="fa fa-pencil"> Dispatched</i></a></li>
                                    <hr>
                                    <li><a href="<?php echo AppInterface::createURL(['product/main/deleteorder', 'id' => $data['order_id']]); ?>" 
                                           class="on-default remove-row"><i class="fa fa-trash-o"> Delete</i></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- end: page -->
</div> <!-- end Panel -->
