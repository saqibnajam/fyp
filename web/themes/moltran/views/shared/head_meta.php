<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php

use app\components\AppInterface;

if ($this->blocks['page_title'] != NULL && !empty($this->blocks['page_title'])) {
    $title = $this->blocks['page_title'];
} elseif (Yii::$app->controller->id == 'site') {
    $title = Yii::$app->controller->module->requestedAction->id;
} else {
    $title = Yii::$app->controller->module->module->requestedAction->id;
}
?>
<title>
    <?php echo AppInterface::getAppName(); ?> -
    <?php echo $title; ?>
</title>


<?php
$url = AppInterface::getBaseUrl();
?>

<!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>/assets/plugins/jquery.steps/demo/css/jquery.steps.css">

<!-- jQuery  -->
<script src="<?php echo $url; ?>/assets/js/jquery.min.js"></script>

<script src="<?php echo $url; ?>/assets/js/bootstrap.min.js"></script>
<!--fav-icon-->
<link rel="shortcut icon" href="<?php echo $url; ?>/assets/images/shoppingcart2.png" type="image/x-icon" />
<!--fav-icon-->
<link href="<?php echo $url; ?>/assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="<?php echo $url; ?>/assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/custom.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/components.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/pages.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/menu.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/plugins/toggles/toggles.css" rel="stylesheet" type="text/css">
<!--        <link href="assets/plugins/toggles/toggles.css" rel="stylesheet">-->
<!--data tables--> 
<link href="<?php echo $url; ?>/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $url; ?>/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $url; ?>/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $url; ?>/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $url; ?>/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--end data tables--> 

<!--bootstrap-wysihtml5-->
<link href="<?php echo $url; ?>/assets/plugins/summernote/dist/summernote.css" rel="stylesheet">

<script src="<?php echo $url; ?>/assets/js/modernizr.min.js"></script>

<!-- Table-editable Css-->
<link rel="stylesheet" href="<?php echo $url; ?>/assets/plugins/magnific-popup/dist/magnific-popup.css">
<link rel="stylesheet" href="<?php echo $url; ?>/assets/plugins/jquery-datatables-editable/datatables.css">
