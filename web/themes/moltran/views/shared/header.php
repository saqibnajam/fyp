<?php

use yii\helpers\Html;
use app\components\AppInterface;
use yii\widgets\Menu;

/* @var $this yii\web\View */
?>
<!-- Top Bar Start -->
<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <p>    
                <a href="<?php echo AppInterface::createURL('site/dashboard'); ?>" class="logo">
                    <img src="<?php echo AppInterface::getBaseUrl(); ?>/assets/images/shoppingcart2.png"
                         alt="<?php echo AppInterface::getAppName(); ?>" title="<?php echo AppInterface::getAppName(); ?>" style="height: 50px;width: 185px;">
                </a>
            </p>
        </div>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left">
                        <i class="fa fa-bars" id="hide"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="md md-crop-free"></i></a>
                    </li>
                    <li class="dropdown">
                        <!---*** Remove this and use AppInterface Method to get the path  -->
                        <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                            <img src="<?php echo AppInterface::getUserImage(); ?>" 
                                 alt="user-img" class="thumb-md img-circle"> 
                        </a>
                        <!---***-->

                        <?php
                        echo Menu::widget([
                            'items' => [
                                // Important: you need to specify url as 'controller/action',
                                // not just as 'controller' even if default action is used.
                                ['label' => '<i class="md md-face-unlock"></i> Profile</a>', 'url' => AppInterface::createURL('user/main/profile')],
                                ['label' => '<i class="md md-vpn-key"></i> Change Password</a>', 'url' => AppInterface::createURL('user/main/password')],
                                ['label' => '<i class="md md-settings-power"></i> Logout</a>', 'url' => AppInterface::createURL('user/main/logout')],
                            ], 'options' => ['class' => 'dropdown-menu'], 'encodeLabels' => false
                        ]);
                        ?>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#hide").click(function() {
            $("p").toggle();
        });
    });
</script>
