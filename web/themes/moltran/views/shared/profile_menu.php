<?php

use yii\widgets\Menu;
use app\components\AppInterface;

echo Menu::widget([
    'items' => [
        ['label' => '<i class="md md-face-unlock"></i> Profile</a>',
            'url' => AppInterface::createURL('user/main/profile')],
        ['label' => '<i class="md md-settings-power"></i> Logout</a>',
            'url' => AppInterface::createURL('user/main/login')],
    ], 'options' => ['class' => 'dropdown-menu'], 'encodeLabels' => false
]);
?>