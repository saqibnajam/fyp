<?php

use yii\helpers\Html;
use app\components\AppInterface;
use yii\widgets\Menu;
use yii\widgets\ActiveForm;
use app\modules\user\components\AppUser;

/* @var $this yii\web\View */
?>
<!-- Top Bar Start -->
<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <p>    
                <a href="<?php echo AppInterface::createURL('site/dashboard'); ?>" class="logo">
                    <img src="<?php echo AppInterface::getBaseUrl(); ?>/assets/images/shoppingcart2.png"
                         alt="<?php echo AppInterface::getAppName(); ?>" title="<?php echo AppInterface::getAppName(); ?>" class="img-thumbnail logo-img">
                </a>
            </p>
        </div>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <!--                <div class="pull-left">
                                    <button class="button-menu-mobile open-left">
                                        <i class="fa fa-bars" id="hide"></i>
                                    </button>
                                    <span class="clearfix"></span>
                                </div>-->
                <form class="navbar-form pull-left" action="dashboard" name="search" method="get">
                    <div class="form-group">
                        <input type="text" value="" name="search" class="form-control search-bar" placeholder="Type here for search...">
                    </div>
                    <button type="submit" class="btn btn-custom"><i class="fa fa-search">search</i></button>
                </form>
                <ul class="nav navbar-nav navbar-right pull-right">
                    <?php if (count(Yii::$app->session['order']) > 0) { ?>
                        <li class="hidden-xs">
                            <a href="<?php echo AppInterface::createURL('site/cart'); ?>"> 
                                <?php
                                $user = AppUser::getCurrentUser();
                                if (empty($user)) {
                                    ?>
                                    <button onclick="checkUser()" style="margin: 10px 40px 0 0;" class="btn btn-danger waves-effect waves-light btn-lg m-b-5"><i class="fa fa-shopping-cart"></i> <span>SHOPPING CART - <?php echo count(Yii::$app->session['order']); ?> item(s)</span></button>
                                <?php } else { ?> 
                                    <button style="margin: 10px 40px 0 0;" class="btn btn-danger waves-effect waves-light btn-lg m-b-5"><i class="fa fa-shopping-cart"></i> <span>SHOPPING CART - <?php echo count(Yii::$app->session['order']); ?> item(s)</span></button>                            
                                <?php } ?> 
                            </a>
                        </li>
                    <?php } ?>
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="md md-crop-free"></i></a>
                    </li>
                    <?php if (\app\modules\user\components\AppUser::isUserLogin()) { ?>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                                <img src="<?php echo AppInterface::getUserImage(); ?>" 
                                     alt="user-img" class="thumb-md img-circle"> 
                            </a>
                            <?php
                            echo Menu::widget([
                                'items' => [
                                    ['label' => '<i class="ion ion-android-social-user"></i> Profile</a>', 'url' => AppInterface::createURL('user/main/profile')],
                                    ['label' => '<i class="fa  fa-cart-arrow-down"></i> My Order(s)</a>', 'url' => AppInterface::createURL('user/main/histry')],
                                    ['label' => '<i class="fa  fa-gift"></i> My lucky draws Gift</a>', 'url' => AppInterface::createURL('user/main/usergift')],
                                    ['label' => '<i class="md md-vpn-key"></i> Change Password</a>', 'url' => AppInterface::createURL('user/main/password')],
                                    ['label' => '<i class="md md-settings-power"></i> Logout</a>', 'url' => AppInterface::createURL('user/main/logout')],
                                ], 'options' => ['class' => 'dropdown-menu'], 'encodeLabels' => false
                            ]);
                            ?>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a href="<?php echo AppInterface::createURL('user/main/login'); ?>"> 
                                <button class="btn btn-danger waves-effect waves-light"><span>Login</span></button> 
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo AppInterface::createURL('user/main/signup'); ?>"> 
                                <button class="btn btn-danger waves-effect waves-light"><span>Signup</span></button> 
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>
                                        $(document).ready(function () {
                                            $("#hide").click(function () {
                                                $("p").toggle();
                                            });
                                        });
</script>
