
<footer class="footer text-right">
    <?php echo Yii::$app->params['footer_txt']; ?>
</footer>

<?php $url = \app\components\AppInterface::getBaseUrl(); ?>

<script>
    var resizefunc = [];
</script>

<!--Form Validation-->
<script src="<?php echo $url; ?>/assets/plugins/bootstrapvalidator/dist/js/bootstrapValidator.js" type="text/javascript"></script>

<!--Form Wizard-->
<script src="<?php echo $url; ?>/assets/plugins/jquery.steps/build/jquery.steps.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

<!--wizard initialization-->
<script src="<?php echo $url; ?>/assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

<!-- jQuery  -->
<script src="<?php echo $url; ?>/assets/js/detect.js"></script>
<script src="<?php echo $url; ?>/assets/js/fastclick.js"></script>
<script src="<?php echo $url; ?>/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo $url; ?>/assets/js/jquery.blockUI.js"></script>
<script src="<?php echo $url; ?>/assets/js/waves.js"></script>


<script src="<?php echo $url; ?>/assets/js/wow.min.js"></script>
<script src="<?php echo $url; ?>/assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo $url; ?>/assets/js/jquery.scrollTo.min.js"></script>

<!--<script type="text/javascript" src="<?php // echo $url;    ?>/assets/plugins/select2/dist/js/select2.min.js"></script>-->

<!--form validation init-->

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/summernote/dist/summernote.min.js"></script>

<script type="text/javascript" src="<?php echo $url; ?>/assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>


<script src="<?php echo $url; ?>/assets/js/jquery.app.js"></script> 

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script src="<?php echo $url; ?>/assets/js/custom.js"></script>


<script src="<?php echo $url; ?>/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/toggles/toggles.min.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/assets/plugins/colorpicker/bootstrap-colorpicker.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="<?php echo $url; ?>/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!--SWEET ALERT--> 
<script src="<?php echo $url; ?>/assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo $url; ?>/assets/pages/jquery.sweet-alert.init.js"></script>



<!--flash Message-->
<script> $(".flashMessage").delay(4200).fadeOut(400);</script>



<!-- data table -->
<script src="<?php echo $url; ?>/assets/plugins/isotope/dist/isotope.pkgd.min.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $url; ?>/assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script> 
<script src="<?php echo $url; ?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo $url; ?>/assets/pages/datatables.editable.init.js"></script>
<script>
    $(document).ready(function () {
        $("table").on('error.dt', function (e, settings, techNote, message) {
            console.log('An error has been reported by DataTables: ', message);
        }).DataTable({
            columnDefs: [{width: '10%', targets: -1}],
            fixedColumns: true
        });
    });
</script>