<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php

use app\components\AppInterface;

$url = AppInterface::getBaseUrl();
?>
<script src="<?php echo $url; ?>/assets/js/jquery.min.js"></script>
<link href="<?php echo $url; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<link href="<?php echo $url; ?>/assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/components.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/pages.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/menu.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
<link href="<?php echo $url; ?>/assets/plugins/jquery-multi-select/multi-select.css" type="text/css">
<link href="<?php echo $url; ?>/assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">





<script src="<?php echo $url; ?>/assets/js/modernizr.min.js"></script>
<!--end--> 


<script>var baseUrl = "<?php echo Yii::$app->request->baseUrl; ?>";</script>
<link rel="icon" href="<?php echo $this->theme->pathMap['@app/views']; ?>/png/apple-touch-icon.png">
<!-- WEB FONTS -->
<link href='//fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic' rel='stylesheet' type='text/css'>
<!-- JQUERY -->
