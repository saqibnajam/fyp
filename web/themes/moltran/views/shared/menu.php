<!-- ========== Left Sidebar Start ========== -->
<?php

use app\components\AppInterface;
use app\modules\user\components\AppUser;
use app\modules\privilege\components\PrivilegeComponent;
?>
<?php
$privileges = Yii::$app->session->get('privileges');
$menu = array(
    array(
        'text' => 'Dashboard',
        'icon' => 'ion ion-home',
        'link' => AppInterface::createURL('site/dashboard'),
        'active' => true,
        'children' => array(),
        'access' => true,
    ),
    array(
        'text' => 'Staff',
        'icon' => 'ion ion-android-contact ',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'Add Staff',
                'link' => AppInterface::createURL('user/main/add'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'View All Staff',
                'link' => AppInterface::createURL('user/main/index'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
    array(
        'text' => 'Customer',
        'icon' => 'ion ion-android-friends ',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'View All Staff',
                'link' => AppInterface::createURL('user/main/list'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
    array(
        'text' => 'Products',
        'icon' => 'ion ion-ipad',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'Add Product',
                'link' => AppInterface::createURL('product/main/add'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'Add Product Type',
                'link' => AppInterface::createURL('product/main/type'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'View All Products',
                'link' => AppInterface::createURL('product/main/index'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'View All Products Type',
                'link' => AppInterface::createURL('product/main/typeindex'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
    array(
        'text' => 'Orders',
        'icon' => 'ion ion-ios7-cart',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'View All Orders',
                'link' => AppInterface::createURL('product/main/order'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
//    array(
//        'text' => 'Sale',
//        'icon' => 'fa fa-buysellads',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//            array(
//                'text' => 'Add Keywords',
//                'link' => AppInterface::createURL('business/keyword/add'),
//                'active' => true,
//                'access' => true,
//            ),
//        ),
//    ),
//    array(
//        'text' => 'Shipment',
//        'icon' => 'ion ion-paper-airplane',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//            array(
//                'text' => 'Add Feeds',
//                'link' => AppInterface::createURL('business/feeds/add'),
//                'active' => true,
//                'access' => true,
//            ),
//        ),
//    ),
//    array(
//        'text' => 'Payment',
//        'icon' => 'ion ion-social-usd ',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//            array(
//                'text' => 'Add Social',
//                'link' => AppInterface::createURL('business/social/add'),
//                'active' => true,
//                'access' => true,
//            ),
//        ),
//    ),
    array(
        'text' => 'Offers',
        'icon' => 'ion ion-ios7-pricetags',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'Add Offer',
                'link' => AppInterface::createURL('product/main/addoffer'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'List Offer',
                'link' => AppInterface::createURL('product/main/indexoffer'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
    array(
        'text' => 'Gifts',
        'icon' => 'fa fa-gift',
        'link' => '#',
        'active' => true,
        'access' => true,
        'children' => array(
            array(
                'text' => 'Add Gift',
                'link' => AppInterface::createURL('site/addgift'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'List Gift',
                'link' => AppInterface::createURL('site/giftindex'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'lucky draws',
                'link' => AppInterface::createURL('site/luckydraws'),
                'active' => true,
                'access' => true,
            ),
            array(
                'text' => 'lucky Customers',
                'link' => AppInterface::createURL('site/giftuser'),
                'active' => true,
                'access' => true,
            ),
        ),
    ),
//    array(
//        'text' => 'Settings',
//        'icon' => 'md md-settings',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(
//        ),
//    ),
//    array(
//        'text' => 'Reporting',
//        'icon' => 'fa fa-file-text',
//        'link' => '#',
//        'active' => true,
//        'access' => true,
//        'children' => array(),
//    ),
);
?>

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div class="user-details">
            <div class="pull-left">
                <img src="<?php echo AppInterface::getUserImage(); ?>" 
                     alt="user-img" class="thumb-md img-circle">
            </div>
            <div class="user-info">
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <?php echo \Yii::$app->user->identity->f_name; ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo AppInterface::createURL('user/main/profile'); ?>">
                                <i class="md md-face-unlock"></i>
                                Profile
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo AppInterface::createURL('user/main/password'); ?>">
                                <i class="md md-vpn-key"></i>
                                Change Password
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo AppInterface::createURL('user/main/logout'); ?>">
                                <i class="md md-settings-power"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>

                <p class="text-muted m-0"><?php echo \Yii::$app->user->identity->type; ?></p>
             </div>
        </div>
        <div id="sidebar-menu">
            <ul>
                <?php
                foreach ($menu as $menuItem) {
                    $active = 0;
                    if ($menuItem['children']) {
                        foreach ($menuItem['children'] as $subMenuItem) {
                            if (Yii::$app->request->url == $subMenuItem['link']) {
                                $active = 1;
                                break;
                            }
                        }
                    }
                    if ($menuItem['active']) {
                        if ($menuItem['children'] == NULL && empty($menuItem['children']) && $menuItem['access']) {
                            ?>
                            <li>
                                <?php
                                if (Yii::$app->request->url == $menuItem['link']) {
                                    ?>
                                    <a href="<?php echo $menuItem['link']; ?>" class="waves-effect active">
                                    <?php } else { ?>
                                        <a href="<?php echo $menuItem['link']; ?>" class="waves-effect">
                                        <?php } ?>
                                        <i class="<?php echo $menuItem['icon']; ?>"></i>
                                        <span> <?php echo $menuItem['text']; ?> </span>
                                    </a>
                            </li>
                        <?php } else { ?>
                            <li class="has_sub">
                                <?php if ($active != 0) { ?>
                                    <a href="#" class="waves-effect subdrop active">
                                    <?php } else { ?>
                                        <a href="#" class="waves-effect">
                                        <?php } ?>
                                        <i class="<?php echo $menuItem['icon']; ?>"></i>
                                        <span> <?php echo $menuItem['text']; ?> </span>
                                        <span class="pull-right">
                                            <i class="md md-add"></i>
                                        </span>
                                    </a>
                                    <?php if ($menuItem['children'] != NULL) { ?>
                                        <ul class="list-unstyled">
                                            <?php
                                            foreach ($menuItem['children'] as $menuSubItem) {
                                                if ($menuSubItem['active'] && $menuSubItem['access'] && Yii::$app->request->url == $menuSubItem['link']) {
                                                    ?> 
                                                    <li class="active"><a href="<?php echo $menuSubItem['link']; ?>" >
                                                            <?php echo $menuSubItem['text']; ?></a>
                                                    </li>
                                                <?php } else {
                                                    ?>
                                                    <li><a href="<?php echo $menuSubItem['link']; ?>" >
                                                            <?php echo $menuSubItem['text']; ?></a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </ul>
                                    <?php } ?> 
                            </li>
                            <?php
                        }
                    }
                }
                ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End --> 

