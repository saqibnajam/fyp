<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="flashMessage">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #317eeb;color: white;">
        <button aria-hidden="true" data-dismiss="alert" class="close" style="opacity: 1;" type="button">×</button>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-error alert-dismissable" style="   
         /*color: red !important;*/
         /*background-color: lightcoral !important;*/
         /*border-color: red !important;*/
         ">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
</div>