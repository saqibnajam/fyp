<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

class AppMessages extends \yii\base\Component {

//     ************ GENERAL MESSAGS ***************** 
    public static $missing_fields = 'Input fields are missing';
    public static $error = 'Cannot be created';
    public static $upload_path = 'Upload path received';
    public static $update_error = 'Cannot be updated';
    public static $create_success = 'Successfully created';
    public static $update_success = 'Successfully updated';
    public static $delete_success = 'Successfully deleted';
    public static $delete_type_success = 'Successfully deleted, Along with all products with this type';
    public static $delete_error = 'Cannot be deleted';
    public static $invalid_HTTP_Method = 'Invalid W/S method';
    public static $no_record = 'No record found';
    public static $settings = 'Settings found';
    public static $business_found = 'Business found';
    public static $device_bus_empty = 'Device not assigned to any business';
    public static $device_empty = 'Device Identifier is empty';
    public static $no_settings = 'No settings found';
    public static $ads = 'Advertise(s) found';
    public static $no_ads = 'No Advertise found';
    public static $terms = 'Terms conditions updated';
    public static $ad_success = 'Ad created successfully';
    public static $ad_failure = 'Ad creation unsuccessfull';
    public static $select_role = 'Select any role';
    public static $location_missing = 'Longitude and latitude is missing';
    public static $image_dimension = 'Image dimension must match';
    public static $buss_offer_error = 'Business code or user offer id is wrong';
    public static $time_error = 'End time should be less than valid till time';
    
//     **********************************************


    public static $priv_created = 'New Privilege has been created successfully';
    public static $priv_deleted = 'Privilege has been deleted successfully';
//    *********** USER RELATED MESSAGES ****************** 
//    public static $signup_success = 'Your account has been created successfully and a verification email sent to your email address';
    public static $signup_success = 'Your account has been created successfully,you can now login with this account';
    public static $user_exist = 'Username or email address already exist';
    public static $user_not_exist = 'Email Address You Provide Does Not Exist.';
    public static $email_exist = 'Email Address You Provide is Already Exist.';
    public static $user_found = 'User found';
    public static $add_success = 'Create customer successfully';
    public static $add_failure = 'Customer can not be created';
    public static $add_success_roles = 'User added successfully. Default privileges have been assigned for one day';
    public static $active_success = 'User active successfully';
    public static $inactive_success = 'User in-active successfully';
    public static $login_success = 'User logged in successfully';
    public static $login_failure = 'Username or Post Code is invalid';
    public static $login_fail_loc = 'Cannot logged in due to latitude longitude';
    public static $reset_success = 'New password has been sent to your email address.';
    public static $reset_failure = 'Email address is invalid';
    public static $password_change = 'Password updated successfully';
    public static $password_failure = 'Password update unsuccessful';
    public static $old_password = 'Old password does not match';
    public static $token_expire = 'Your session expired, please login again';
    public static $profile_success = 'Profile updated successfully';
    public static $profile_failure = 'Profile updated unsuccessful';
    public static $under_verification = 'Your account is not verified yet';
    public static $image_success = 'Image uploaded successfully';
    public static $image_failure = 'Image upload unsuccessful';
//    ******************************************************
//    
//    
//    *************   OFFER MESSAGES  ******************** 
    public static $offers_found = 'Offers found';
    public static $redeem_success = 'Offer redeemed successfully';
    public static $redeem_fail = 'Offer redeemed unsuccessfull';
    public static $offers_not_found = 'No Offers found or you\'ve already subscribed to this offer';
    public static $offer_created = 'Offer created successfully';
    public static $offer_invalid = 'Offer invalid or already exists';
//    ****************************************************
//    ************   KEYWORDS AND BUSINESS MESSAGES ********************

    public static $soc_add_success = 'Social added successfully';
    public static $soc_edit_success = 'Social edited successfully';
    public static $soc_add_error = 'Social cannot be added';
    public static $soc_edit_error = 'Social cannot be edited';
    public static $key_add_success = 'Keywords added successfully';
    public static $key_add_error = 'Keywords cannot be added';
    public static $key_edit_error = 'Keywords cannot be updated';
    public static $key_edit_success = 'Keywords updated successfully';
    public static $keywords_found = 'Keywords found';
    public static $business_created = 'Business created successfully';
    public static $social_add_success = 'Social added successfully';
    public static $social_add_error = 'Social cannot be added';
    public static $social_found = 'Social URLs found';
    public static $feeds_found = 'Feeds found';
    public static $no_feeds_found = 'No feeds found';
    public static $no_keywords_found = 'No keywords found';
    public static $no_social_url_found = 'No social urls found';
    public static $wrong_business_id = 'Wrong business ID or business ID is empty';
//    ************   DEVICE MESSAGES ********************

    public static $dev_add_success = 'Device added successfully';
    public static $dev_add_error = 'Device cannot be added';
    public static $dev_edit_success = 'Device updated successfully';
    public static $dev_edit_error = 'Device cannot be updated';
    public static $dev_not_found = 'Device not found';
//    ************   FEEDS MESSAGES ********************

    public static $feed_add_success = 'Feed added successfully';
    public static $feed_add_error = 'Feed cannot be added';
    public static $feed_edit_success = 'Feed updated successfully';
    public static $feed_edit_error = 'Feed cannot be updated';

}
