<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use yii\helpers\Url;
use app\modules\business\models\BusinessSocial;
use app\modules\business\models\Social;
use app\models\Country;
use app\models\Ads;
use app\models\Currency;
use yii\web\UploadedFile;
use app\modules\user\components\AppUser;
use yii\data\ActiveDataProvider;
use app\modules\business\models\BusinessAds;

class AppInterface extends \yii\base\Component {

    public static function getArrayIndex($arr) {
        if (sizeof($arr) > 0) {
            foreach ($arr as $a) {
                if (!empty($a) AND $a != NULL AND $a != 0) { // NULL, 0
                    $new_array[] = $a;
                }
            }
        }
        return $new_array;
    }

    public static function getModelError($errors) {
        foreach ($errors as $error) {
            return \Yii::$app->getSession()->setFlash('error', $error[0]);
        }
        return NULL;
    }

    public static function getBaseUrl() {
        return \Yii::$app->getRequest()->baseUrl . '/themes/moltran';
    }

    public static function getCountry($id) {
        return Country::find()->where(['id' => $id])->one();
    }

    public static function getCurrency($id) {
        return Currency::find()->select('id,title,code,exchange_rate,is_default')->where(['id' => $id])->one();
    }

    public static function getAppName() {
        return 'Online Grocery';
    }

    public static function createURL($path) {
        return \Yii::$app->urlManager->createUrl($path);
    }

    public static function uploadImage($asset, $model, $folder) {
        if ($asset != null) {
            $full_path = \Yii::$app->basePath . '/web/uploads/' . $folder . '/image';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
//                $model->image = \Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $folder . '/image') . '/' . $img_name;
                $model->image = $img_name;
                if ($model->update()) {
                    return FLAG_UPDATED;
                } else {
                    return FLAG_NOT_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function uploadVideo($asset, $model, $folder) {
        if ($asset != null) {
            $full_path = \Yii::$app->basePath . '/web/uploads/' . $folder . '/video';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
                $model->file = \Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $folder . '/video') . '/' . $img_name;
                if ($model->update()) {
                    return FLAG_UPDATED;
                } else {
                    dd($model->errors);
                    return FLAG_NOT_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function getUniqueID() {
        $id = microtime(true) - '1400000000';
        $id += ip2long(self::getClientIP());
        $id -= self::generateRandomNumber(8);
        if (strpos($id, '.')) {
            list($whole, $fraction) = explode('.', $id);
            $id = $whole - $fraction;
        }
        return abs($id);
    }

    private static function generateRandomNumber($length) {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }

    public static function getClientIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function deleteAsset($path, $file_name) {
        return unlink(\Yii::$app->basePath . $path . '/' . $file_name) ? 1 : 0;
    }

    public static function getRequestHeader($value) {
        $request = \Yii::$app->request->headers;
//        dd($request);
        if ($request[$value] != '' || !empty($request[$value])) {
            return $request[$value];
        }
    }

    public static function getParamValue($value) {
        return \Yii::$app->params[$value];
    }

    public static function getUploadPath($type = '', $token = '') {
        $uploadPath = [];
        $uploadPath['file_name'] = self::getFilename();
        $uploadPath['host'] = \Yii::$app->params['upload']['host'][$type];
        $token = self::getParamValue('dropbox') != '' ? self::getParamValue('dropbox') : $token;
        $uploadPath['token'] = self::hashToken($token);
        $uploadPath['path'] = \Yii::$app->params['upload']['path'][$type];
        $uploadPath['type'] = $type;
        return $uploadPath;
    }

    public static function getAds($argv) {
        $ads_calc = [];
        $business_ads = BusinessAds::find()->where(['business_id' => $argv['business_id']])->all();
        if (count($business_ads) > 0) {
            $ads_calc = self::adsCalculation($business_ads);
        }
        return $ads_calc;
    }

    public static function adsCalculation($ads) {
        $lowest = [];
        $min_freq = [];
        $ids = [];
        for ($j = 0; $j < count($ads); $j++) {
            $lowest[$j]['id'] = $ads[$j]->id;
            $lowest[$j]['frequency'] = $ads[$j]->frequency;
            $lowest[$j]['location'] = $ads[$j]->ad->location;
            $lowest[$j]['offer_id'] = $ads[$j]->ad->offer_id;
//            $lowest[$j]['ad'] = $ads[$j]->ad;
            $min_freq[] = $ads[$j]->frequency;
        }
//       *******  to get the lowest frequency
        $lowest_value = min($min_freq);
        $max_count = max($min_freq) / $lowest_value;
        for ($i = 0; $i < count($lowest); $i++) {
            if ($lowest[$i]['frequency'] / $lowest_value > 1) {
                for ($j = 1; $j < $lowest[$i]['frequency'] / $lowest_value; $j++) {
                    $ids[$lowest[$i]['location']][] = $lowest[$i]['offer_id'];
                }
            } else {
                $ids[$lowest[$i]['location']][] = $lowest[$i]['offer_id'];
            }
        }
        return $ids;
    }

//    public static function getAds($argv) {
//        
//        ///*** Add Pagination in this W/S
//        $result = [];
//        $query = Ads::find()->select('id,offer_id,location')->where(['status' => Ads::ACTIVE]);
//        $count = count($query);
//        $keywords = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pageSize' => $argv['pageSize'],
//                'page' => $argv['page'],
//                'totalCount' => $count,
//            ],
//        ]);
//        $result['Page'] = $argv['page'] + 1;
//        $result['Total_count'] = $count;
//        if (count($keywords->getModels()) > 0) {
//            $result['Result'] = $keywords->getModels();
//        } else {
//            return;
//        }
//        return $result;
//    }

    public static function uploadAsset($type, $file_name, $path, $image) {
        if ($image != null) {
            $full_path = \Yii::$app->basePath . '/' . $path;
            $asset = UploadedFile::getInstanceByName('asset');
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                return FLAG_UPDATED;
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function hashToken($token) {
        return base64_encode($token);
    }

    public static function decryptToken($token) {
        return base64_decode($token);
    }

    public static function getImageType($base) {
        $type = substr($base, 0, 5);
        if ($type == 'R0lGO') {
            $type = 'gif';
        } else if ($type == '/9j/4') {
            $type = 'jpg';
        } else {
            $type = 'png';
        }
        return $type;
    }

    public static function generateToken($len = 16) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $len);
        return $randomString;
    }

    public static function generateTokenInNumber($len = 16) {
        $randomString = substr(str_shuffle("0123456789"), 0, $len);
        return $randomString;
    }

    public static function getFilename($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                    substr($charid, 8, 4);


            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

    public static function getDirRealPath() {
        return Yii::getPathOfAlias('webroot') . '/';
    }

    public static function resizeUserImg($path, $filename) {

        $photoPth = $path . '/' . $filename;
        $orgPth = $path . '/original/' . $filename;
        $thumbPth = $path . '/thumb/' . $filename;

        self::copyImage($photoPth, $orgPth);
        self::resizeImage($orgPth, $photoPth, 500);
        self::resizeImage($orgPth, $thumbPth, 200);
    }

    public static function resizeImage($source, $destination, $size) {

        $extArr = explode('.', $source);
        $type = "jpg";

        if (isset($ext[1]))
            $type = $ext[1];

        $image = ImageEditor::createFromFile($source);
        $image->decreaseTo($size);
        $image->save($destination, $type, 100);
    }

    public static function copyImage($source, $destination) {
        copy($source, $destination);
    }

    public static function saveImg64Encoded($path, $encodedImg) {

        $img = str_replace('data:image/png;base64,', '', $encodedImg);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        return file_put_contents($path, $data);
    }

    public static function getAbsoluteUrl($path, $params, $scheme = 'http') {
        return Url::to(array_merge([$path], $params), $scheme);
    }

    public static function getCurrentTime() {
        return time();
    }

    public static function getUserImage() {
        if (isset(\Yii::$app->user->identity->image)) {
            $image = \Yii::$app->user->identity->image;
            $new = explode("image/", $image);
            if ($image && file_exists(\Yii::$app->basePath . '/web/uploads/user/image/' . $new[1])) {
                $img = $image;
            } else {
                $img = self::getBaseUrl() . '/assets/images/users/avatar-1.jpg';
            }
            return $img;
        } else {
            return self::getBaseUrl() . '/assets/images/users/avatar-1.jpg';
        }
    }

    public static function getMonthCount($data, $type) {
        $arr = array();
        $count = 0;
        for ($i = 0; $i < count($data); $i++) {
            if ($type == 'created_at') {
                $month = date('M', $data[$i]->created_at);
            } elseif ($type == 'modified_at') {
                $month = date('M', $data[$i]->modified_at);
            }
            if ($month == 'Jan') {
                if (!isset($arr['Jan'])) {
                    $arr['Jan'] = 0;
                    $arr['Jan'] += 1;
                } else {
                    $arr['Jan'] += 1;
                }
            } elseif ($month == 'Feb') {
                if (!isset($arr['Feb'])) {
                    $arr['Feb'] = 0;
                    $arr['Feb'] += 1;
                } else {
                    $arr['Feb'] += 1;
                }
            } elseif ($month == 'Mar') {
                if (!isset($arr['Mar'])) {
                    $arr['Mar'] = 0;
                    $arr['Mar'] += 1;
                } else {
                    $arr['Mar'] += 1;
                }
            } elseif ($month == 'Apr') {
                if (!isset($arr['Apr'])) {
                    $arr['Apr'] = 0;
                    $arr['Apr'] += 1;
                } else {
                    $arr['Apr'] += 1;
                }
            } elseif ($month == 'May') {
                if (!isset($arr['May'])) {
                    $arr['May'] = 0;
                    $arr['May'] += 1;
                } else {
                    $arr['May'] += 1;
                }
            } elseif ($month == 'Jun') {
                if (!isset($arr['Jun'])) {
                    $arr['Jun'] = 0;
                    $arr['Jun'] += 1;
                } else {
                    $arr['Jun'] += 1;
                }
            } elseif ($month == 'Jul') {
                if (!isset($arr['Jul'])) {
                    $arr['Jul'] = 0;
                    $arr['Jul'] += 1;
                } else {
                    $arr['Jul'] += 1;
                }
            } elseif ($month == 'Aug') {
                if (!isset($arr['Aug'])) {
                    $arr['Aug'] = 0;
                    $arr['Aug'] += 1;
                } else {
                    $arr['Aug'] += 1;
                }
            } elseif ($month == 'Sep') {
                if (!isset($arr['Sep'])) {
                    $arr['Sep'] = 0;
                    $arr['Sep'] += 1;
                } else {
                    $arr['Sep'] += 1;
                }
            } elseif ($month == 'Oct') {
                if (!isset($arr['Oct'])) {
                    $arr['Oct'] = 0;
                    $arr['Oct'] += 1;
                } else {
                    $arr['Oct'] += 1;
                }
            } elseif ($month == 'Nov') {
                if (!isset($arr['Nov'])) {
                    $arr['Nov'] = 0;
                    $arr['Nov'] += 1;
                } else {
                    $arr['Nov'] += 1;
                }
            } elseif ($month == 'Dec') {
                if (!isset($arr['Dec'])) {
                    $arr['Dec'] = 0;
                    $arr['Dec'] += 1;
                } else {
                    $arr['Dec'] += 1;
                }
            }
        }
        return $arr;
    }

}

?>
