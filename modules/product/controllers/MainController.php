<?php

namespace app\modules\product\controllers;

use app\components\AccessRule;
use yii\filters\AccessControl;
use app\components\AppInterface;
use app\modules\product\models\Product;
use app\modules\product\models\ProductType;
use app\components\AppMessages;
use app\modules\product\components\AppProduct;
use yii\web\UploadedFile;
use app\modules\user\components\AppUser;

class MainController extends \yii\web\Controller {

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $types = ProductType::find()->orderby(['modified_at' => SORT_DESC])->all();
        $model = new Product();
        if ($_POST) {
            $product = AppProduct::productCreateUpdate($model, 'create', $_POST['Product']);
            if ($product->save()) {
                if (isset($_FILES) && $_FILES != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Product[image]'), $product, 'product');
                }
                \Yii::$app->getSession()->setFlash('success', AppMessages::$create_success);
                return $this->redirect(['index']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$error);
            }
        }
        return $this->render('add', array('model' => $model, 'types' => $types));
    }

    public function actionAddoffer() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $products = Product::find()->orderby(['title' => SORT_ASC])->all();
        $model = new \app\modules\product\models\Offers();
        if ($_POST) {
            $offer = AppProduct::offerCreateUpdate($model, 'create', $_POST['Offers']);
            if ($offer) {
                \Yii::$app->getSession()->setFlash('success', AppMessages::$create_success);
                return $this->redirect(['indexoffer']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$error);
            }
        }
        return $this->render('add_offer', array('model' => $model, 'products' => $products));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $types = ProductType::find()->orderby(['modified_at' => SORT_DESC])->all();
        $model = Product::findOne($id);
        if ($_POST) {
            $product = AppProduct::productCreateUpdate($model, 'update', $_POST['Product']);
            if ($product->save()) {
                if (isset($_FILES['Product']['name']['image']) && $_FILES['Product']['name']['image'] != null) {
                    $image = AppInterface::uploadImage(UploadedFile::getInstanceByName('Product[image]'), $product, 'product');
                }
                \Yii::$app->getSession()->setFlash('success', AppMessages::$update_success);
                return $this->redirect(['index']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$update_error);
            }
        }
        return $this->render('edit', array('model' => $model, 'types' => $types));
    }

    public function actionType() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new ProductType();
        if ($_POST) {
            $model->id = AppInterface::getUniqueID();
            $model->title = $_POST['ProductType']['title'];
            $model->description = $_POST['ProductType']['description'];
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', AppMessages::$create_success);
                return $this->redirect(['typeindex']);
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$error);
            }
        }
        return $this->render('type', array('model' => $model));
    }

    public function actionTypeindex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = ProductType::find()->orderby(['modified_at' => SORT_DESC])->all();
        return $this->render('typeindex', array('model' => $model));
    }

    public function actionTypedelete($id) {
        $exist = Product::find()->where(['type_id' => $id])->all();
//        dd($exist);
        foreach ($exist as $data) {
            $data->delete();
        }
        $model = ProductType::findOne($id);
        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_type_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect(['typeindex']);
    }

    public function actionDelete($id) {
        $model = Product::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect(['index']);
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Product::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = Product::find()->where(['is_deleted' => 0])->orderby(['modified_at' => SORT_ASC])->all();
        return $this->render('index', array('model' => $model));
    }

    public function actionIndexoffer() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Offers::find()->where(['is_deleted' => 0])->orderby(['modified_at' => SORT_ASC])->all();
        return $this->render('offer_index', array('model' => $model));
    }

    public function actionOrder() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Shipment::find()->where(['is_deleted' => 0])->orderby(['issue_date' => SORT_DESC])->all();
        return $this->render('order', array('model' => $model));
    }

    public function actionOrderdetail($id) {
        $user = AppUser::getCurrentUser();
        if (isset($user->id)) {
            $layout = $user->type == 'customer' ? 'dashboard' : 'default';
        } else {
            $layout = 'dashboard';
        }
        $this->layout = \Yii::$app->params['layout_path'] . $layout;
        $model = \app\modules\product\models\Shipment::find()->where(['order_id' => $id])->all();
        $order = \app\modules\product\models\Order::find()->where(['order_id' => $model[0]['order_id']])->all();

        $offers = \app\modules\product\models\Offers::find()->where(['is_deleted' => 0])->all();
        $offer_arr = [];
        foreach ($offers as $data) {
            $offer_product = \app\modules\product\models\OfferProduct::find()->where(['offer_id' => $data['id']])->all();
            foreach ($offer_product as $key => $item) {
                if ($data['status'] == 'active') {
                    $offer_arr[$key] = $item['product_id'];
                }
            }
        }

        return $this->render('detail', array('model' => $model, 'order' => $order, 'offers' => $offer_arr));
    }

    public function actionUpdatestatus($id, $status) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Shipment::find()->where(['order_id' => $id])->one();
        $model['status'] = $status;
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$update_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$update_error);
        }
//        $model = \app\modules\product\models\Shipment::find()->where(['is_deleted' => 0])->orderby(['modified_at' => SORT_DESC])->all();
        return $this->redirect('order');
    }

    public function actionOfferstatus($id, $type) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Offers::find()->where(['id' => $id])->one();
        $model['status'] = $type;
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$update_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$update_error);
        }
        return $this->redirect('indexoffer');
    }

    public function actionDeleteorder($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Shipment::find()->where(['order_id' => $id])->one();
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect('order');
    }

    public function actionDeleteoffer($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = \app\modules\product\models\Offers::find()->where(['id' => $id])->one();
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect('indexoffer');
    }

}
