<?php

/*
 * Description: Component for User Module
 * Author: 
 * Dated:  2-June-2014
 */

namespace app\modules\product\components;

use app\components\AppInterface;
use yii\web\UploadedFile;

class AppProduct extends \yii\base\Component {

    public static function productCreateUpdate($model, $type, $data) {
        if ($type == 'create') {
            $model->id = AppInterface::getUniqueID();
            $model->attributes = $data;
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
        } elseif ($type == 'update') {
            $model->attributes = $data;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
        }
        return $model;
    }

    public static function offerCreateUpdate($model, $type, $data) {
        if ($type == 'create') {
            $model->id = AppInterface::getUniqueID();
            $model->attributes = $data;
            $model->created_at = AppInterface::getCurrentTime();
            $model->created_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
            if ($model->save()) {
                foreach ($data['product'] as $product) {
                    $model1 = new \app\modules\product\models\OfferProduct();
                    $model1->id = AppInterface::getUniqueID();
                    $model1->offer_id = $model->id;
                    $model1->product_id = $product;
                    $model1->created_by = 1;
                    $model1->modified_by = 1;
                    $model1->created_at = AppInterface::getCurrentTime();
                    $model1->modified_at = AppInterface::getCurrentTime();
                    $model1->save();
                }
                return TRUE;
            } else {
                return FALSE;
            }
        } elseif ($type == 'update') {
            $model->attributes = $data;
            $model->modified_at = AppInterface::getCurrentTime();
            $model->modified_by = \app\modules\user\components\AppUser::getCurrentUser()->id;
        }
        return $model;
    }

}

?>
