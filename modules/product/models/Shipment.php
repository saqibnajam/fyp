<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "og_shipment".
 *
 * @property string $id
 * @property string $order_id
 * @property string $customer_id
 * @property string $issue_date
 * @property string $delivery_date
 * @property string $status
 * @property string $description
 * @property string $f_name
 * @property string $l_name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $city_id
 * @property string $state_id
 * @property string $country_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 * @property integer $is_deleted
 * @property string $payment_type
 * @property string $card_type
 * @property string $card_number
 * @property string $expiry_month
 * @property string $expiry_year
 * @property string $card_verification
 *
 * @property OgUser $createdBy
 * @property OgUser $modifiedBy
 * @property Order $order 
 */
class Shipment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'og_shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'required'],
            [['id', 'order_id', 'customer_id', 'issue_date', 'delivery_date', 'city_id', 'state_id', 'country_id', 'created_at', 'modified_at', 'created_by', 'modified_by', 'is_deleted'], 'integer'],
            [['status', 'description', 'payment_type'], 'string'],
            [['f_name', 'l_name', 'phone'], 'string', 'max' => 64],
            [['email', 'card_type', 'card_number', 'expiry_month', 'expiry_year', 'card_verification'], 'string', 'max' => 128],
            [['address'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'customer_id' => 'Customer ID',
            'issue_date' => 'Issue Date',
            'delivery_date' => 'Delivery Date',
            'status' => 'Status',
            'description' => 'Description',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'city_id' => 'City ID',
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'is_deleted' => 'Is Deleted',
            'payment_type' => 'Payment Type',
            'card_type' => 'Card Type',
            'card_number' => 'Card Number',
            'expiry_month' => 'Expiry Month',
            'expiry_year' => 'Expiry Year',
            'card_verification' => 'Card Verification',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(OgUser::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(OgUser::className(), ['id' => 'modified_by']);
    }

    public function getOrder() {
        return $this->hasMany(Order::className(), ['order_id' => 'order_id']);
    }

}
