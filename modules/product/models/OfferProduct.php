<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "og_offer_product".
 *
 * @property string $id
 * @property string $product_id
 * @property string $offer_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * @property OgProduct $product
 * @property OgOffers $offer
 */
class OfferProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'og_offer_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'offer_id'], 'required'],
            [['id', 'product_id', 'offer_id', 'created_at', 'modified_at', 'created_by', 'modified_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'offer_id' => 'Offer ID',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(OgProduct::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(OgOffers::className(), ['id' => 'offer_id']);
    }
}
