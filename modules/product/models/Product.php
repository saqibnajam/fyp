<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "og_product".
 *
 * @property string $id
 * @property string $title
 * @property string $type_id
 * @property string $buy_price
 * @property string $retail_price
 * @property string $wholesale_price
 * @property string $image 
 * @property string $description
 * @property string $status
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * @property CustomerGift[] $CustomerGifts
 * @property Order[] $Orders
 * @property User $createdBy
 * @property User $modifiedBy
 * @property ProductType $type
 */
class Product extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'og_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'title', 'type_id'], 'required'],
            [['id', 'type_id', 'is_deleted', 'created_at', 'modified_at', 'created_by', 'modified_by'], 'integer'],
            [['buy_price', 'retail_price', 'wholesale_price'], 'number'],
            [['description', 'status'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type_id' => 'Type ID',
            'buy_price' => 'Buy Price',
            'retail_price' => 'Retail Price',
            'wholesale_price' => 'Wholesale Price',
            'image' => 'Image',
            'description' => 'Description',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerGifts() {
        return $this->hasMany(CustomerGift::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType() {
        return $this->hasOne(ProductType::className(), ['id' => 'type_id']);
    }
}
