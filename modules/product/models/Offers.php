<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "og_offers".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * @property OgOfferProduct[] $ogOfferProducts
 * @property OgUser $createdBy
 * @property OgUser $modifiedBy
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'og_offers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'is_deleted', 'created_at', 'modified_at', 'created_by', 'modified_by'], 'integer'],
            [['description', 'status'], 'string'],
            [['title'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOgOfferProducts()
    {
        return $this->hasMany(OgOfferProduct::className(), ['offer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(OgUser::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(OgUser::className(), ['id' => 'modified_by']);
    }
}
