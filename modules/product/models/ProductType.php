<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "og_product_type".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 */
class ProductType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'og_product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'created_at', 'created_by', 'modified_at', 'modified_by'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
        ];
    }
}
