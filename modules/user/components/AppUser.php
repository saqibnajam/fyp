<?php

/*
 * Description: Component for User Module
 * Author: 
 * Dated:  2-June-2014
 */

namespace app\modules\user\components;

use app\components\AppMailer;
use app\components\AppInterface;
use app\components\AppSetting;
use app\modules\privilege\components\PrivilegeComponent;
use app\modules\user\models\User;
use app\modules\user\models\UserRole;
use app\modules\user\models\UserOffers;
use app\modules\offers\models\Offers;
use app\modules\business\models\Keywords;
use app\modules\business\models\BusinessKeyword;
use app\modules\business\models\BusinessFeeds;
use app\modules\business\models\Business;
use app\modules\business\models\Feeds;
use yii\web\UploadedFile;
use yii\helpers\Url;
use app\models\Cms;
use app\modules\user\models\Role;
use yii\data\ActiveDataProvider;
use app\modules\privilege\models\UserPrivileges;
use app\modules\product\models\Order;
use app\modules\product\models\Shipment;
use app\modules\product\models\Product;
use Yii;

class AppUser extends \yii\base\Component {

    public static function getUser($email) {
        $user = User::findOne(['email' => $email]);
        if ($user != '' || $user != NULL) {
            return $user;
        }
    }

    public static function getUserByParam($param = '', $value = '') {
        $user = User::findOne([$param => $value]);
        if ($user != '' || $user != NULL) {
            return $user;
        }
    }

    public static function getCurrentUser() {
        return \Yii::$app->user->identity;
    }

    public static function getRecommendations() {
        self::getCurrentUser();
        $user = self::getCurrentUser();
        $shipment = Shipment::find()->joinWith('order')
                ->where(['is_deleted' => 0, 'customer_id' => $user->id])
                ->all();
        $notIn = implode(",", Yii::$app->session['order']);
        if ($shipment) {
//            d('if');
            $in = '';
            foreach ($shipment as $s_key => $data) {
                if (isset($data['order'])) {
                    foreach ($data['order'] as $key => $item) {
                        $last = count($data['order']) - 1;
                        if ($last == $key) {
                            $in .= "'" . $item['product_id'] . "'";
                        } else {
                            $in .= "'" . $item['product_id'] . "',";
                        }
                    }
                }
                $last_ship = count($shipment) - 1;
//                d($last_ship);
//                dd($s_key);
                if ($last_ship != $s_key) {
//                    $in .= ",";
                }
            }
            if (count($shipment) > 1) {
                $in .= ",'0'";
            }
//            dd($in);
            $sql = "SELECT * FROM og_product WHERE id IN (" . $in . ") AND id NOT IN (" . $notIn . ")";
        } else {
//            dd('else');
            $order = Yii::$app->session['order'];
            $in = '';
            foreach ($order as $key => $data) {
                $product = Product::find()->where(['id' => $data])->one();
                $last = count($order) - 1;
                if ($last == $key) {
                    $in .= "'" . $product['type_id'] . "'";
                } else {
                    $in .= "'" . $product['type_id'] . "',";
                }
            }
            $sql = "SELECT * FROM og_product WHERE type_id IN (" . $in . ") AND id NOT IN (" . $notIn . ")";
        }
//        dd($sql);
        $recommendations = Product::findBySql($sql)->all();
        return $recommendations;
    }

    public static function getUserId() {
        return \Yii::$app->user->identity->id;
    }

    public static function getCurrentUserRoleId() {
        return \Yii::$app->user->identity->userRoles[0]->role_id;
    }

    public static function getUserRoleId($id) {
        return User::findOne(['id' => $id])->userRoles[0]->role_id;
    }

    public static function isUserLogin() {

        if (\Yii::$app->user->isGuest)
            return false;
        else {
            return true;
        }
    }

    public static function authenticate($email, $postal_code) {
        $user = User::find()->select('id,f_name,l_name,description,email,token,dob,state,image,city,postal_code,login_count,local_offers,third_party_offers,verification_code,last_login,modified_at,gender')
                ->where(['email' => $email, 'LOWER(REPLACE(postal_code," ",""))' => strtolower(str_replace(' ', '', $postal_code))])
                ->one();

        if ($user != '' || $user != NULL) {
            return $user;
        }

        return null;
    }

    public static function userOffersCount($user_id) {
        $count = 0;
        if ($user_id != null || $user_id != '') {
            $user_offers = UserOffers::find()->where(['user_id' => $user_id])->all();
            if (count($user_offers) > 0) {
                $count = count($user_offers);
            }
        }

        return $count;
    }

    public static function createUserRole($user_id, $role_id, $flag = User::WEB) {
        $role = new UserRole();
        $role->id = AppInterface::getUniqueID();
        $role->role_id = $role_id;
        $role->user_id = $user_id;
        $role->created_at = AppInterface::getCurrentTime();
        if ($flag == User::WEB) {
            $role->created_by = AppUser::getUserId();
        } else {
            $role->created_by = 1;
        }
        if ($role->save()) {
            if ($role_id != Role::user) {
                $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role->id, $role_id);
//                $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role_id);
                if (count($assign_priv) > 0) {
                    return FLAG_UPDATED;
                } else {
                    return FLAG_UPDATED;
                }
            }
        } else {
            return $role->errors;
        }
    }

    public static function updateUserRole($user_id, $role_id) {
        $role = UserRole::find()->where(['user_id' => $user_id])->one();
        if (isset($role)) {
            if ($role_id != '0') {
                $role->role_id = $role_id;
            }
            $del = UserPrivileges::deleteAll(['user_id' => $user_id]);
            if ($role->update()) {
                if ($role_id != '5') {
                    $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role->id, $role_id);
//                    $assign_priv = PrivilegeComponent::updateUserRolePrivilege($user_id, $role_id);
                    if (count($assign_priv) > 0) {
                        return FLAG_UPDATED;
                    } else {
                        return FLAG_UPDATED;
                    }
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        } else {
            $role = self::createUserRole($user_id, $role_id);
            return FLAG_UPDATED;
        }
    }

    public static function isUserSuperAdmin() {
        $user = \Yii::$app->user->identity;
        if ($user->userRoles[0]->role->id == Role::super_admin) {
            return true;
        } else {
            return false;
        }
    }

    public static function addButtonCheck() {
        $user = \Yii::$app->user->identity;
        ///*** Proper methodlogy to cater user roles ( You cannot use title or id at the same time. ID would be perfect to handle but use it 
        //through constant)
        if ($user->userRoles[0]->role->id == Role::super_admin || $user->userRoles[0]->role->id == Role::business_admin ||
                $user->userRoles[0]->role->id == Role::sales_representative) {
            return true;
        } else {
            return false;
        }
    }

    public static function isUserSalesRep() {
        $user = \Yii::$app->user->identity;
        if ($user->userRoles[0]->role->id == Role::sales_representative) {  ///*** Use proper constants here and should be in Role Model
            return true;
        } else {
            return false;
        }
    }

    public static function getUserCompanyIds() {
        $user = \Yii::$app->user->identity;
        $ids = null;
        if ($user->userRoles[0]->role->id == Role::business_admin) {
            $sub = self::getSubCompanyIds(\Yii::$app->session->get('business_id'));
            foreach ($sub as $subb) {
                $ids .= $subb->id . ',';
            }
            $ids .= \Yii::$app->session->get('business_id');
        } elseif ($user->userRoles[0]->role->id == Role::branch_admin) {
            
        }
        return $ids;
    }

    public static function getCustomerIds() {
        $ids = null;
        $sub = UserRole::find()->select('user_id')->where(['role_id' => 3])->all();
        foreach ($sub as $subb) {
            $ids .= $subb->user_id . ',';
        }
        $ids .= 0;
        return $ids;
    }

    public static function getUserIds() {
        $ids = null;
        $sub = UserRole::find()->select('user_id')->where(['role_id' => 2])->all();
        foreach ($sub as $subb) {
            $ids .= $subb->user_id . ',';
        }
        $ids .= 0;
        return $ids;
    }

    public static function getSubCompanyIds($parent_id) {
        $sub_business = Business::find()->select('id')->where(['parent_id' => $parent_id])->all();
        return $sub_business;
    }

    public static function uploadImage($asset, $model) {
        if ($asset != null) {
            $full_path = \Yii::$app->basePath . '/web/uploads/user/image';
            $file_name = AppInterface::getFilename();
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                $img_name = $file_name . '.' . $asset->getExtension();
                $user = User::find()->where(['id' => $model->id])->one();
                $user->image = $img_name;
                if ($user->update()) {
                    return FLAG_UPDATED;
                }
            } else {
                return FLAG_NOT_UPDATED;
            }
        }
    }

    public static function sendCodeToAdmin(&$user) {
        $business = Business::find()->where(['id' => $user->business_id])->one();
        if ($business) {
            $mailer = new AppMailer();
            $mailer->prepareBody(Cms::business_email, array('PIN_NUMBER' => $business->business_code,
                    )
            );
            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->f_name)), array('email' => AppSetting::getSettingsValueByKey('SUPPORT_EMAIL'),
                'name' => AppSetting::getSettingsValueByKey('SUPPORT_NAME')));
        }
    }

    public static function createCheckout($data) {
        $order_id = AppInterface::getUniqueID();
        $model2 = new Shipment();
        $model2->attributes = $data;
        $model2->id = AppInterface::getUniqueID();
        $model2->order_id = $order_id;
        $model2->customer_id = AppUser::getCurrentUser()->id;
        $model2->issue_date = AppInterface::getCurrentTime();
        $model2->delivery_date = AppInterface::getCurrentTime() + 86400 * 3;
        $model2->status = 'pending';
        $model2->created_by = AppUser::getCurrentUser()->id;
        $model2->modified_by = AppUser::getCurrentUser()->id;
        $model2->created_at = AppInterface::getCurrentTime();
        $model2->modified_at = AppInterface::getCurrentTime();
        if ($model2->save()) {
            $order = Yii::$app->session['order'];
//            dd($order);
            foreach ($order as $product) {
                $model1 = new Order();
                $model1->id = AppInterface::getUniqueID();
                $model1->order_id = $order_id;
                $model1->product_id = $product;
                $model1->created_by = AppUser::getCurrentUser()->id;
                $model1->modified_by = AppUser::getCurrentUser()->id;
                $model1->created_at = AppInterface::getCurrentTime();
                $model1->modified_at = AppInterface::getCurrentTime();
                $model1->save();
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function createUser($user, $type) {
//        dd($user);
        $model = new User();
        $model->id = AppInterface::getUniqueID();
        $model->attributes = $user;
        $model->password = self::pwdHash($user['password']);
        if (isset($user['dob'])) {
            $model->dob = strtotime($user['dob']);
        }
        $model->type = $type;
        $model->created_by = 1;
        $model->modified_by = 1;
        $model->created_at = AppInterface::getCurrentTime();
        $model->modified_at = AppInterface::getCurrentTime();
        if ($model->save(FALSE)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function isUserExists($email) {
        return User::find()->where(['email' => $email])->count() > 0 ? true : false;
    }

    public static function changePassword(&$user, $old, $new) {
        $status = FLAG_NOT_UPDATED;
        if (self::pwdHash($old) == $user->password) {
            $user->password = self::pwdHash($new);
            $user->modified_at = AppInterface::getCurrentTime();
            $user->scenario = User::SCENARIO_CHG_PWD;
            if ($user->save()) {
                $status = FLAG_UPDATED;
            } else {
                $status = $user->errors;
            }
        } else {
            $status = FLAG_ERROR;
        }
        return $status;
    }

    public static function updateUser(&$user, $update_field) {
        $user->attributes = $update_field;
        $user->modified_at = AppInterface::getCurrentTime();
        $user->modified_by = AppUser::getCurrentUser()->id;
        if (isset($update_field['password']) && $update_field['password'] != '') {
            $user->password = self::pwdHash($update_field['password']);
        }
        if ($user->update()) {
            $status = TRUE;
        } else {
            $status = FALSE;
        }
        return $status;
    }

//    ********* updated replica of this function is now in AppInterface names as uploadAsset, this should be deleted after code review
    public static function updateImage($type, $file_name, $path, $image) { ///*** It should be UploadAsset and should be place in AppInterface
        if ($image != null) {
            $full_path = \Yii::$app->basePath . '/' . $path;
            $asset = UploadedFile::getInstanceByName('asset');
            if ($asset->saveAs($full_path . '/' . $file_name . '.' . $asset->getExtension())) {
                return FLAG_UPDATED;
            } else {
                return FLAG_NOT_UPDATED;
            }
//            $filename = $file_name . '.' . $user->image->extension; ///*** It should be dynamic extension
//                $user->image->saveAs($path . $filename); ///*** CUseFileUploader
//            }
        }
    }

    public static function updateUserImage(&$user, $file_name, $type) {
        $user->image = $file_name;
        $user->scenario = User::SCENARIO_IMAGE;
        if ($user->save()) {
            return FLAG_UPDATED;
        } else {
            return $user->errors;
        }
    }

    public static function updateModelImage(&$model, $file_name) {
        $model->image = $file_name;
        $model->scenario = $model::SCENARIO_IMAGE;
        if ($model->save()) {
            return FLAG_UPDATED;
        } else {
            return $model->errors;
        }
    }

    public static function updateCount(&$user) {
        $status = FLAG_NOT_UPDATED;
        $user->login_count = $user->login_count + 1;
        $user->last_login = AppInterface::getCurrentTime();
        $user->modified_at = AppInterface::getCurrentTime();
        $user->scenario = User::SCENARIO_COUNT;
        if ($user->save()) {
            $status = FLAG_UPDATED;
        } else {
            $status = $user->errors;
        }
        return $status;
    }

    public static function isTokenValid($token) {
        return User::find()->where(['token' => $token])->count() ? true : false;
    }

    public static function checkBusinessId($id) {
        return Business::find()->where(['id' => $id])->count() ? true : false;
    }

    public static function availOffers($user, $argv) {
        $result = [];
        $query = Offers::find()->asArray()->select('cp_offers.*,cp_user_offers.status,cp_user_offers.offer_code')
                ->joinWith(['userOffers' => function($q) use ($user) {
                        $q->select('cp_user_offers.*')->where('cp_user_offers.user_id = ' . $user->id);
                    }])->
                andWhere('cp_offers.valid_till > ' . time());
//        $query = UserOffers::find()->asArray()->select('cp_offers.*,cp_user_offers.status,cp_user_offers.offer_code')
//                ->join('INNER JOIN', 'cp_offers', 'cp_offers.id=cp_user_offers.offer_id')
//                ->where(['cp_user_offers.user_id' => $user->id])->andWhere('cp_offers.valid_till > '.  time());
//        $query = UserOffers::find()->asArray()
//                    ->select('cp_user_offers.*')
////                    ->leftJoin('cp_offers', 'cp_user_offers.offer_id = cp_offers.id')
//                    ->where(['cp_user_offers.status' => 'approve'])
//                    ->andWhere('cp_offers.valid_till > '.  time())
////                    ->andWhere('')
//                    ->with('offer');
        $off = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $argv['pageSize'],
                'page' => $argv['page'],
                'totalCount' => $query->count(),
            ],
        ]);
        $result['Page'] = $argv['page'] + 1;
        $result['Total_count'] = $query->count();
        if (count($off->getModels()) > 0) {
            $result['Result'] = $off->getModels();
        } else {
            return;
        }
        return $result;
    }

    public static function updateResetCode(&$user) {
        $user->reset_code = AppInterface::generateToken();
        $user->modified_at = AppInterface::getCurrentTime();
        if ($user->save()) {
            return FLAG_UPDATED;
        } else {
            return $user->errors;
        }
    }

    public static function sendResetLink(&$user) {
        $status = FLAG_NOT_UPDATED;
        $response_code = self::updateResetCode($user);
        if ($response_code == FLAG_UPDATED) {
//            $mailer = new AppMailer();
            $password = AppInterface::generateToken();
            $user->password = self::pwdHash($password);
            $user->update();
            $link = \Yii::$app->urlManager->baseUrl . '/user/main/login';
//            $mailer->prepareBody('ResetPassword', array('NAME' => $user->f_name, 'LOGIN_URL' => $link, 'EMAIL' => $user->email
//                , 'PASSWORD' => $password));
//            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->f_name))
//                    , array('email' => AppSetting::getSettingsValueByKey('SUPPORT_EMAIL'),
//                'name' => AppSetting::getSettingsValueByKey('SUPPORT_NAME')));
            $status = FLAG_UPDATED;
        } else {
            $status = $response_code;
        }
        return $status;
    }

    public static function pwdHash($input) {
        return md5($input);
    }

}

?>
