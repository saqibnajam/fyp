<?php

namespace app\modules\user\models;

use app\modules\user\models\Role;
use app\modules\user\models\UserOffers;
use app\modules\user\models\UserRole;
use app\modules\business\models\Business;
use app\modules\business\models\BusinessDevices;
use app\modules\business\models\BusinessKeyword;
use app\modules\privilege\models\Privilege;
use app\modules\privilege\models\PrivilegeFunction;
use app\modules\privilege\models\PrivilegeGroup;
use app\models\Country;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "og_user".
 *
 * @property string $id
 * @property string $f_name
 * @property string $l_name
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property string $dob 
 * @property string $phone
 * @property string $image
 * @property string $address
 * @property string $description
 * @property string $city_id 
 * @property string $state_id 
 * @property string $country_id 
 * @property string $type
 * @property string $reset_code
 * @property integer $is_registered 
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * @property OgGifts[] $ogGifts
 * @property OgGifts[] $ogGifts0
 * @property OgOffers[] $ogOffers
 * @property OgOffers[] $ogOffers0
 * @property OgOffersType[] $ogOffersTypes
 * @property OgOffersType[] $ogOffersTypes0
 * @property OgProduct[] $ogProducts
 * @property OgProduct[] $ogProducts0
 * @property OgProductPrice[] $ogProductPrices
 * @property OgProductPrice[] $ogProductPrices0
 * @property OgShipment[] $ogShipments
 * @property OgShipment[] $ogShipments0
 * @property User $createdBy
 * @property User[] $users
 * @property User $modifiedBy
 * @property User[] $users0
 * @property OgCountry $country
 * @property OgStates $state
 * @property OgCities $city
 * @property OgUserGift[] $ogUserGifts
 */
class User extends ActiveRecord implements IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'og_user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'f_name', 'email', 'password'], 'required'],
            [['id', 'dob', 'city_id', 'state_id', 'country_id', 'is_registered', 'is_deleted', 'created_at', 'modified_at', 'created_by', 'modified_by'], 'integer'],
            [['gender', 'description', 'type'], 'string'],
            [['f_name', 'l_name', 'email', 'password', 'phone', 'image', 'reset_code'], 'string', 'max' => 64],
            [['address'], 'string', 'max' => 128],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'f_name' => 'First Name',
            'l_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'gender' => 'Gender',
            'dob' => 'Dob',
            'phone' => 'Phone',
            'image' => 'Image',
            'address' => 'Address',
            'description' => 'Description',
            'type' => 'Type',
            'city_id' => 'City ID',
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
            'reset_code' => 'Reset Code',
            'is_registered' => 'Is Registered',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS    from BSOURCECODE* */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGifts() {
        return $this->hasMany(Gifts::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers0() {
        return $this->hasMany(Offers::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffersTypes0() {
        return $this->hasMany(OffersType::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts0() {
        return $this->hasMany(Product::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices0() {
        return $this->hasMany(ProductPrice::className(), ['modified_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipments() {
        return $this->hasMany(Shipment::className(), ['created_by' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return self::find()->where(['id' => $id])->one();
    }

    /* modified */

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByEmail($email) {
        return User::findOne(['email' => $email
//                if user is_deleted is 1 so it will not be able to login
                    , 'is_deleted' => '0'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password, $user) {
        return md5($password) === $user->password;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(OgCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState() {
        return $this->hasOne(OgStates::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity() {
        return $this->hasOne(OgCities::className(), ['id' => 'city_id']);
    }

}
