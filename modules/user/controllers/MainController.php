<?php

namespace app\modules\user\controllers;

use app\components\AccessRule;
use app\components\AppInterface;
use yii\web\Controller;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\components\AppUser;
use app\components\AppMessages;
use app\models\Country;
use app\models\State;
use app\models\City;
use yii\web\UploadedFile;

class MainController extends \yii\web\Controller {

    public function actionHistry() {
        $this->layout = \Yii::$app->params['layout_path'] . 'dashboard';
        $user = AppUser::getCurrentUser();
        $model = \app\modules\product\models\Shipment::find()->where(['is_deleted' => 0, 'customer_id' => $user->id])->orderby(['modified_at' => SORT_DESC])->all();
        return $this->render('histry', array('model' => $model));
    }

    public function actionIndex() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = User::find()->where(['is_deleted' => 0, 'type' => 'staff'])->all();
        return $this->render('index', array('model' => $model));
    }

    public function actionList() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = User::find()->where(['is_deleted' => 0, 'type' => 'customer'])->all();
        return $this->render('list', array('model' => $model));
    }

    public function actionPassword() {
        $user = AppUser::getCurrentUser();
        if (isset($user->id)) {
            $layout = $user->type == 'admin' ? 'default' : 'dashboard';
        } else {
            $layout = 'dashboard';
        }
        $this->layout = \Yii::$app->params['layout_path'] . $layout;
        $model = User::findOne(['id' => AppUser::getUserId()]);
        if (isset($_POST['User']) && isset($_POST['User']) != null) {
            if ($model->password != md5($_POST['User']['old_password'])) {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$old_password);
                return $this->redirect(['password']);
            } else {
                $model->password = AppUser::pwdHash($_POST['User']['password']);
                if ($model->update()) {
                    \Yii::$app->getSession()->setFlash('success', AppMessages::$password_change);
                    return $this->redirect(['profile']);
                } else {
                    \Yii::$app->getSession()->setFlash('success', AppMessages::$password_failure);
                    return $this->redirect(['password']);
                }
            }
        }
        return $this->render('password', array('model' => $model));
    }

    public function actionLogin() {
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        $model = new LoginForm();
        if (isset($_POST) && $_POST != null) {
            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                return $this->redirect('../../site/dashboard');
            }
        }
        return $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        \Yii::$app->user->logout();

        return $this->redirect('login');
    }

    public function actionSignup() {
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        $model = new User();
        $countries = Country::find()->all();
        if ($model->load(\Yii::$app->request->post())) {
            if (isset($_POST['User']['email'])) {
                $email = $_POST['User']['email'];
                if (AppUser::isUserExists($email) != true) {
                    $model = AppUser::createUser($_POST['User'], 'customer');
                    if ($model == true) {
                        \Yii::$app->getSession()->setFlash('success', AppMessages::$add_success);
                        return $this->redirect(['login']);
                    } else {
                        \Yii::$app->getSession()->setFlash('error', AppMessages::$add_failure);
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('error', AppMessages::$email_exist);
                }
            }
        }
        return $this->render('signup', array('model' => $model, 'countries' => $countries));
    }

    public function actionAdd() {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = new User();
        $countries = Country::find()->all();
        $states = State::find()->all();
        $cities = City::find()->all();

        if ($model->load(\Yii::$app->request->post())) {
            if (isset($_POST['User']['email'])) {
                $email = $_POST['User']['email'];
                if (AppUser::isUserExists($email) != true) {
                    $model = AppUser::createUser($_POST['User'], 'staff');
                    if ($model == true) {
                        \Yii::$app->getSession()->setFlash('success', AppMessages::$add_success);
                        return $this->redirect(['index']);
                    } else {
                        \Yii::$app->getSession()->setFlash('error', AppMessages::$add_failure);
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('error', AppMessages::$email_exist);
                }
            }
        }
        return $this->render('add', array('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
    }

    public function actionEdit($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $countries = Country::find()->all();
        $states = State::find()->all();
        $cities = City::find()->all();

        if (isset($id)) {
            $model = User::find()->where(['id' => $id])->one();
            if ($model != null || !empty($model)) {
                if (isset($_POST['User'])) {
                    $update = AppUser::updateUser($model, $_POST['User']);
                    if ($update == TRUE) {
                        \Yii::$app->getSession()->setFlash('success', AppMessages::$update_success);
                        return $this->redirect(['index']);
                    }
                }
            }
        }
        return $this->render('edit', array('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
    }

    public function actionView($id) {
        $this->layout = \Yii::$app->params['layout_path'] . 'default';
        $model = User::find()->where(['id' => $id])->one();
        return $this->render('view', array('model' => $model));
    }

    public function actionDelete($id, $type) {
        $model = User::findOne($id);
        $model->is_deleted = '1';
        if ($model->update()) {
            \Yii::$app->getSession()->setFlash('success', AppMessages::$delete_success);
        } else {
            \Yii::$app->getSession()->setFlash('error', AppMessages::$delete_error);
        }
        return $this->redirect([$type]);
    }

    public function actionProfile() {
        $user = AppUser::getCurrentUser();
        if (isset($user->id)) {
            $layout = $user->type == 'admin' ? 'default' : 'dashboard';
        } else {
            $layout = 'dashboard';
        }
        $this->layout = \Yii::$app->params['layout_path'] . $layout;
        $countries = Country::find()->all();
        $states = State::find()->all();
        $cities = City::find()->all();

        $model = User::find()->where(['id' => AppUser::getUserId()])->one();
        if ($model != null || !empty($model)) {
            if (isset($_POST['User'])) {
                $image = UploadedFile::getInstanceByName('User[image]') != NULL ? UploadedFile::getInstanceByName('User[image]') : null;
                $update = AppUser::updateUser($model, $_POST['User'], $image);
            }
            if (isset($update)) {
                \Yii::$app->getSession()->setFlash('success', AppMessages::$update_success);
                return $this->redirect('profile');
            }
        }
        return $this->render('edit_profile', array('model' => $model, 'countries' => $countries, 'states' => $states, 'cities' => $cities));
    }

    public function actionReset() {
        $this->layout = \Yii::$app->params['layout_path'] . 'authorization';
        if (isset($_POST['User']['email'])) {
            $email = $_POST['User']['email'];
            if (AppUser::isUserExists($email) != false) {
                $user = AppUser::getUser($email);
                $reset = AppUser::sendResetLink($user);
                if ($reset == FLAG_UPDATED) {
                    \Yii::$app->getSession()->setFlash('success', AppMessages::$reset_success);
                    return $this->redirect('login');
                } else {
                    \Yii::$app->getSession()->setFlash('success', AppMessages::$reset_failure);
                    return $this->redirect('login');
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', AppMessages::$user_not_exist);
                return $this->redirect('login');
            }
        }
        $model = new User();
        return $this->render('reset', array('model' => $model));
    }

    public function actionGetstate($name) {
        $arr = [];
        $states = State::find()->where(['country_id' => $_POST['country_id']])->orderBy('id')->all();
        $count = 0;
        $dd = ''
                . '<select class=" form-control test" onchange="getcity(this.value)" name=" ' . $name . ' " id="state">';
        $dd .= '<option value="">Select State</option>';
        if (count($states) > 0) {
            foreach ($states as $state) {
                $dd .= '<option value="' . $state->id . '">' . $state->title . '</option>';
            }
        } else {
            
        }
        $dd .= '</select>';
        echo $dd;
    }

    public function actionGetcity($name) {
        $arr = [];
        $cities = City::find()->where(['state_id' => $_POST['state_id']])->orderBy('id')->all();
        $count = 0;
        $dd = '<select class="form-control "  name="' . $name . '" id="city">';
        $dd .= '<option value="">Select City</option>';
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $dd .= '<option value="' . $city->id . '">' . $city->title . '</option>';
            }
        } else {
            
        }$dd .= '</select>';
        echo $dd;
    }

    public function actionUsergift() {
        $this->layout = \Yii::$app->params['layout_path'] . 'Dashboard';
        $model = \app\models\GiftUser::find()->where(['is_deleted' => 0, 'user_id' => AppUser::getCurrentUser()->id])->all();
        return $this->render('gift_user', array('model' => $model));
    }

}
