<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "og_cities".
 *
 * @property string $id
 * @property string $title
 * @property string $state_id
 *
 * @property OgStates $state
 * @property OgUser[] $ogUsers
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'og_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'state_id'], 'required'],
            [['id', 'state_id'], 'integer'],
            [['title'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'state_id' => 'State ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(OgStates::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOgUsers()
    {
        return $this->hasMany(OgUser::className(), ['city_id' => 'id']);
    }
}
